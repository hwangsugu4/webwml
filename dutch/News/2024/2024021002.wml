be#use wml::debian::translation-check translation="9d41ab1625a3bbe9bf95b782d91e91b766a3f664"
<define-tag pagetitle>Debian 11 is bijgewerkt: 11.9 werd uitgebracht</define-tag>
<define-tag release_date>2024-02-10</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.9</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Het Debian-project kondigt met genoegen de negende update aan van oldstable,
zijn voorgaande stabiele distributie Debian <release> (codenaam <q><codename></q>).
Deze tussenrelease voegt voornamelijk correcties voor beveiligingsproblemen toe,
samen met een paar aanpassingen voor ernstige problemen. Beveiligingsadviezen
werden reeds afzonderlijk gepubliceerd en, waar beschikbaar, wordt hiernaar
verwezen.</p>

<p>Merk op dat de tussenrelease geen nieuwe versie van Debian <release> is,
maar slechts een update van enkele van de meegeleverde pakketten. Het is niet
nodig om oude media met <q><codename></q> weg te gooien. Na de installatie
kunnen pakketten worden opgewaardeerd naar de huidige versie door een
bijgewerkte Debian-spiegelserver te gebruiken.</p>

<p>Wie regelmatig updates installeert vanuit security.debian.org zal niet veel
pakketten moeten updaten, en de meeste van dergelijke updates zijn opgenomen in
de tussenrelease.</p>

<p>Nieuwe installatie-images zullen binnenkort beschikbaar zijn op de gewone
plaatsen.</p>

<p>Het upgraden van een bestaande installatie naar deze revisie kan worden
bereikt door het pakketbeheersysteem naar een van de vele HTTP-spiegelservers
van Debian te verwijzen. Een uitgebreide lijst met spiegelservers is
beschikbaar op:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Oplossingen voor diverse problemen</h2>

<p>Met deze update van oldstable, de voorgaande stabiele distributie, worden een paar
belangrijke correcties aangebracht aan de volgende pakketten:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction axis "Niet-ondersteunde protocollen uitfilteren uit de clientklasse ServiceFactory [CVE-2023-40743]">
<correction base-files "Update voor tussenrelease 11.9">
<correction cifs-utils "Reparatie voor niet-parallelle compilaties">
<correction compton "Aanbeveling van picom verwijderen">
<correction conda-package-handling "Onbetrouwbare tests overslaan">
<correction conmon "Niet blijven hangen bij het doorsturen van container stdout/stderr met veel uitvoer">
<correction crun "Oplossing voor containers met systemd als init-systeem bij gebruik van nieuwere kernelversies">
<correction debian-installer "Linux kernel ABI verhogen naar 5.10.0-28; opnieuw bouwen tegen proposed-updates">
<correction debian-installer-netboot-images "Opnieuw bouwen tegen proposed-updates">
<correction debian-ports-archive-keyring "Automatische ondertekeningssleutel voor het archief Debian Ports toevoegen (2025)">
<correction debian-security-support "Markeren dat tor, consul en xen het einde van hun levensduur bereikt hebben; samba-ondersteuning beperken tot niet-AD DC toepassingen; golang-pakketten matchen met reguliere expressie; versiegebaseerd controleren laten vallen; chromium toevoegen aan security-support-ended.deb11; tiles en libspring-java toevoegen aan security-support-limited">
<correction debootstrap "Wijzigingen inzake ondersteuning voor samengevoegd /usr uit trixie toepassen: samengevoegd /usr toepassen via achteraf-samenvoeging, voor suites die recenter zijn dan bookworm de standaard in alle profielen instellen op samengevoegd /usr">
<correction distro-info "Updaten van tests voor distro-info-data 0.58+deb12u1, dat de einde-levensduurdatum van Debian 7 aanpaste">
<correction distro-info-data "Toevoegen van Ubuntu 24.04 LTS Noble Numbat; verschillende einde-levensduurdata aanpassen">
<correction dpdk "Nieuwe bovenstroomse stabiele release">
<correction dropbear "Probleem met omzeilen van beveiligingsmaatregelen oplossen [CVE-2021-36369]; oplossing voor <q>terrapin</q>-aanval [CVE-2023-48795]">
<correction exuberant-ctags "Probleem met willekeurige opdrachtuitvoering oplossen [CVE-2022-4515]">
<correction filezilla "Voorkomen van <q>terrapin</q>-aanval [CVE-2023-48795]">
<correction gimp "Oude versies van apart verpakte dds-plug-in verwijderen">
<correction glib2.0 "In overeenstemming brengen met bovenstroomse stabiele oplossingen; problemen met denial of service oplossen [CVE-2023-32665 CVE-2023-32611 CVE-2023-29499 CVE-2023-32636]">
<correction glibc "Herstellen van een geheugenbeschadiging in <q>qsort()</q> bij gebruik van niet-transitieve vergelijkingsfuncties.">
<correction gnutls28 "Beveiligingsoplossing voor een zijkanaal-timingaanval [CVE-2023-5981]">
<correction imagemagick "Verschillende beveiligingsoplossingen [CVE-2021-20241 CVE-2021-20243 CVE-2021-20244 CVE-2021-20245 CVE-2021-20246 CVE-2021-20309 CVE-2021-3574 CVE-2021-39212 CVE-2021-4219 CVE-2022-1114 CVE-2022-28463 CVE-2022-32545 CVE-2022-32546]">
<correction jqueryui "Probleem met cross-site scripting oplossen [CVE-2022-31160]">
<correction knewstuff "Zorgen voor correcte ProvidersUrl om denial of service op te lossen">
<correction libdatetime-timezone-perl "Meegeleverde tijdzonegegevens bijwerken">
<correction libde265 "Segmentatie-inbreuk verhelpen in de functie <q>decoder_context::process_slice_segment_header</q> [CVE-2023-27102]; heap bufferoverloop oplossen in de functie <q>derive_collocated_motion_vectors</q> [CVE-2023-27103]; oplossen van voorbij buffer lezen in <q>pic_parameter_set::dump</q> [CVE-2023-43887]; oplossen van bufferoverloop in de functie <q>slice_segment_header</q> [CVE-2023-47471]; oplossen van problemen met bufferoverloop [CVE-2023-49465 CVE-2023-49467 CVE-2023-49468]">
<correction libmateweather "Meegeleverde locatiegegevens bijwerken; URL van dataserver bijwerken">
<correction libpod "Foutieve verwerking van aanvullende groepen oplossen [CVE-2022-2989]">
<correction libsolv "Ondersteuning voor zstd-compressie inschakelen">
<correction libspreadsheet-parsexlsx-perl "Mogelijke geheugenbom verhelpen [CVE-2024-22368]; reparatie voor probleem van XML externe entiteitsaanval [CVE-2024-23525]">
<correction linux "Nieuwe bovenstroomse stabiele release; ABI verhogen naar 28">
<correction linux-signed-amd64 "Nieuwe bovenstroomse stabiele release; ABI verhogen naar 28">
<correction linux-signed-arm64 "Nieuwe bovenstroomse stabiele release; ABI verhogen naar 28">
<correction linux-signed-i386 "Nieuwe bovenstroomse stabiele release; ABI verhogen naar 28">
<correction llvm-toolchain-16 "Nieuw pakket met erin geïntegreerde nieuwere ontwikkelingen ter ondersteuning van compilaties van recentere versies van chromium; instellen van <q>llvm-spirv</q> als bouwvereiste in plaats van <q>llvm-spirv-16</q>">
<correction mariadb-10.5 "Nieuwe bovenstroomse stabiele release; oplossing voor probleem van denial of service [CVE-2023-22084]">
<correction minizip "Overflows van zip-headervelden verwerpen [CVE-2023-45853]">
<correction modsecurity-apache "Problemen met het omzeilen van de beveiliging oplossen [CVE-2022-48279 CVE-2023-24021]">
<correction nftables "Foutief genereren van bytecode verhelpen">
<correction node-dottie "Probleem met prototypevervuiling oplossen [CVE-2023-26132]">
<correction node-url-parse "Probleem met het omzeilen van autorisatie oplossen [CVE-2022-0512]">
<correction node-xml2js "Probleem met het omzeilen van autorisatie opgelost [CVE-2023-0842]">
<correction nvidia-graphics-drivers "Nieuwe bovenstroomse release [CVE-2023-31022]">
<correction nvidia-graphics-drivers-tesla-470 "Nieuwe bovenstroomse release [CVE-2023-31022]">
<correction opendkim "Authentication-Results headers correct verwijderen [CVE-2022-48521]">
<correction perl "Bufferoverloop via illegale Unicode-eigenschap voorkomen [CVE-2023-47038]">
<correction plasma-desktop "Denial of service bug in discover repareren">
<correction plasma-discover "Denial of service bug repareren; reparatie voor bouwfout">
<correction postfix "Nieuwe bovenstroomse stabiele release; SMTP smokkelprobleem aanpakken [CVE-2023-51764]">
<correction postgresql-13 "Nieuwe bovenstroomse stabiele release; probleem met SQL-injectie oplossen [CVE-2023-39417]">
<correction postgresql-common "autopkgtests repareren">
<correction python-cogent "Parallelle tests overslaan op systemen met één CPU">
<correction python-django-imagekit "Uitlokken van detectie van path traversal in tests vermijden">
<correction python-websockets "Probleem met voorspelbare duur oplossen [CVE-2021-33880]">
<correction pyzoltan "Bouwen op systemen met één kern">
<correction ruby-aws-sdk-core "Het VERSION-bestand toevoegen aan het pakket">
<correction spip "Probleem met cross-site scripting oplossen">
<correction swupdate "Voorkomen dat beheerdersrechten worden verkregen door ongepaste socketmodus">
<correction symfony "Ervoor zorgen dat de filters van CodeExtension hun invoer op de juiste manier maskeren [CVE-2023-46734]">
<correction tar "Bereikcontrole in base-256 decoder repareren [CVE-2022-48303], hantering van uitgebreide headervoorvoegsels [CVE-2023-39804]">
<correction tinyxml "Assertie-probleem oplossen [CVE-2023-34194]">
<correction tzdata "Meegeleverde tijdzonegegevens bijwerken">
<correction unadf "Probleem met stackbufferoverloop oplossen [CVE-2016-1243]; probleem met het uitvoeren van willekeurige code oplossen [CVE-2016-1244]">
<correction usb.ids "Meegeleverde gegevenslijst bijwerken">
<correction vlfeat "FTBFS met nieuwere ImageMagick repareren">
<correction weborf "Probleem van denial of service oplossen">
<correction wolfssl "Problemen met bufferoverloop verhelpen [CVE-2022-39173 CVE-2022-42905], probleem van onthulling van sleutels [CVE-2022-42961], voorspelbare buffer in invoersleutelmateriaal [CVE-2023-3724]">
<correction xerces-c "Probleem met gebruik na vrijgave oplossen [CVE-2018-1311]; probleem met integeroverloop oplossen [CVE-2023-37536]">
<correction zeromq3 "Oplossing voor <q>fork()</q>-detectie met gcc 7; verklaring van gewijzigde copyright-licentie bijwerken">
</table>


<h2>Beveiligingsupdates</h2>


<p>Met deze revisie worden de volgende beveiligingsupdates toegevoegd aan de release oldstable. Het beveiligingsteam heeft voor elk van deze updates
al een advies uitgebracht:</p>

<table border=0>
<tr><th>Advies-ID</th>  <th>Pakket</th></tr>
<dsa 2023 5496 firefox-esr>
<dsa 2023 5499 chromium>
<dsa 2023 5506 firefox-esr>
<dsa 2023 5508 chromium>
<dsa 2023 5509 firefox-esr>
<dsa 2023 5511 mosquitto>
<dsa 2023 5512 exim4>
<dsa 2023 5513 thunderbird>
<dsa 2023 5514 glibc>
<dsa 2023 5515 chromium>
<dsa 2023 5516 libxpm>
<dsa 2023 5517 libx11>
<dsa 2023 5518 libvpx>
<dsa 2023 5519 grub-efi-amd64-signed>
<dsa 2023 5519 grub-efi-arm64-signed>
<dsa 2023 5519 grub-efi-ia32-signed>
<dsa 2023 5519 grub2>
<dsa 2023 5520 mediawiki>
<dsa 2023 5522 tomcat9>
<dsa 2023 5523 curl>
<dsa 2023 5524 libcue>
<dsa 2023 5526 chromium>
<dsa 2023 5527 webkit2gtk>
<dsa 2023 5528 node-babel7>
<dsa 2023 5530 ruby-rack>
<dsa 2023 5531 roundcube>
<dsa 2023 5533 gst-plugins-bad1.0>
<dsa 2023 5534 xorg-server>
<dsa 2023 5535 firefox-esr>
<dsa 2023 5536 chromium>
<dsa 2023 5537 openjdk-11>
<dsa 2023 5538 thunderbird>
<dsa 2023 5539 node-browserify-sign>
<dsa 2023 5540 jetty9>
<dsa 2023 5542 request-tracker4>
<dsa 2023 5543 open-vm-tools>
<dsa 2023 5544 zookeeper>
<dsa 2023 5545 vlc>
<dsa 2023 5546 chromium>
<dsa 2023 5547 pmix>
<dsa 2023 5548 openjdk-17>
<dsa 2023 5549 trafficserver>
<dsa 2023 5550 cacti>
<dsa 2023 5551 chromium>
<dsa 2023 5554 postgresql-13>
<dsa 2023 5556 chromium>
<dsa 2023 5557 webkit2gtk>
<dsa 2023 5558 netty>
<dsa 2023 5560 strongswan>
<dsa 2023 5561 firefox-esr>
<dsa 2023 5563 intel-microcode>
<dsa 2023 5564 gimp>
<dsa 2023 5565 gst-plugins-bad1.0>
<dsa 2023 5566 thunderbird>
<dsa 2023 5567 tiff>
<dsa 2023 5569 chromium>
<dsa 2023 5570 nghttp2>
<dsa 2023 5571 rabbitmq-server>
<dsa 2023 5572 roundcube>
<dsa 2023 5573 chromium>
<dsa 2023 5574 libreoffice>
<dsa 2023 5576 xorg-server>
<dsa 2023 5577 chromium>
<dsa 2023 5579 freeimage>
<dsa 2023 5581 firefox-esr>
<dsa 2023 5582 thunderbird>
<dsa 2023 5584 bluez>
<dsa 2023 5585 chromium>
<dsa 2023 5586 openssh>
<dsa 2023 5587 curl>
<dsa 2023 5588 putty>
<dsa 2023 5590 haproxy>
<dsa 2023 5591 libssh>
<dsa 2023 5592 libspreadsheet-parseexcel-perl>
<dsa 2024 5594 linux-signed-amd64>
<dsa 2024 5594 linux-signed-arm64>
<dsa 2024 5594 linux-signed-i386>
<dsa 2024 5594 linux>
<dsa 2024 5595 chromium>
<dsa 2024 5597 exim4>
<dsa 2024 5598 chromium>
<dsa 2024 5599 phpseclib>
<dsa 2024 5600 php-phpseclib>
<dsa 2024 5602 chromium>
<dsa 2024 5603 xorg-server>
<dsa 2024 5604 openjdk-11>
<dsa 2024 5605 thunderbird>
<dsa 2024 5606 firefox-esr>
<dsa 2024 5608 gst-plugins-bad1.0>
<dsa 2024 5613 openjdk-17>
<dsa 2024 5614 zbar>
<dsa 2024 5615 runc>
</table>


<h2>Verwijderde pakketten</h2>

<p>Het volgende verouderde pakket werd verwijderd uit de distributie:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction gimp-dds "Geïntegreerd in gimp >=2.10">

</table>

<h2>Het Debian-installatiesysteem</h2>
<p>Het installatiesysteem werd bijgewerkt om de reparaties die met deze tussenrelease in oldstable, de vorige stabiele release, opgenomen werden, toe te voegen.</p>

<h2>URL's</h2>

<p>De volledige lijsten met pakketten die met deze revisie gewijzigd werden:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>De huidige distributie oldstable:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Voorgestelde updates voor de distributie oldstable:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>informatie over de distributie oldstable (notities bij de release, errata, enz.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Beveiligingsaankondigingen en -informatie:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Over Debian</h2>

<p>Het Debian-project is een samenwerkingsverband van ontwikkelaars van vrije
software die vrijwillig tijd en moeite steken in het produceren van het
volledig vrije besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>, stuur een e-mail naar
&lt;press@debian.org&gt;, of neem contact met het release-team voor de stabiele
release op &lt;debian-release@lists.debian.org&gt;.</p>


