# translation of vote.ru.po to Russian
# Yuri Kozlov <yuray@komyakino.ru>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml templates\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2019-12-14 21:36+0500\n"
"Last-Translator: Lev Lamberov <dogsleg@debian.org>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.4\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "Дата"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "Временная шкала"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr "Резюме"

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "Назначения"

#: ../../english/template/debian/votebar.wml:25
msgid "Withdrawals"
msgstr "Отклонено"

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr "Обсуждение"

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr "Платформы"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposer"
msgstr "Выдвигающий предложение"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr "Выдвинувший предложение А"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr "Выдвинувший предложение Б"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr "Выдвинувший предложение В"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr "Выдвинувший предложение Г"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr "Выдвинувший предложение Д"

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr "Выдвинувший предложение Е"

#: ../../english/template/debian/votebar.wml:55
msgid "Proposal G Proposer"
msgstr "Выдвинувший предложение G"

#: ../../english/template/debian/votebar.wml:58
msgid "Proposal H Proposer"
msgstr "Выдвинувший предложение H"

#: ../../english/template/debian/votebar.wml:61
msgid "Seconds"
msgstr "Поддерживающий предложение"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal A Seconds"
msgstr "Поддержавший предложение А"

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal B Seconds"
msgstr "Поддержавший предложение Б"

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal C Seconds"
msgstr "Поддержавший предложение В"

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal D Seconds"
msgstr "Поддержавший предложение Г"

#: ../../english/template/debian/votebar.wml:76
msgid "Proposal E Seconds"
msgstr "Поддержавший предложение Д"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal F Seconds"
msgstr "Поддержавший предложение Е"

#: ../../english/template/debian/votebar.wml:82
msgid "Proposal G Seconds"
msgstr "Поддержавший предложение G"

#: ../../english/template/debian/votebar.wml:85
msgid "Proposal H Seconds"
msgstr "Поддержавший предложение H"

#: ../../english/template/debian/votebar.wml:88
msgid "Opposition"
msgstr "Оппозиция"

#: ../../english/template/debian/votebar.wml:91
msgid "Text"
msgstr "Текст"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal A"
msgstr "Предложение А"

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal B"
msgstr "Предложение Б"

#: ../../english/template/debian/votebar.wml:100
msgid "Proposal C"
msgstr "Предложение В"

#: ../../english/template/debian/votebar.wml:103
msgid "Proposal D"
msgstr "Предложение Г"

#: ../../english/template/debian/votebar.wml:106
msgid "Proposal E"
msgstr "Предложение Д"

#: ../../english/template/debian/votebar.wml:109
msgid "Proposal F"
msgstr "Предложение Е"

#: ../../english/template/debian/votebar.wml:112
msgid "Proposal G"
msgstr "Предложение G"

#: ../../english/template/debian/votebar.wml:115
msgid "Proposal H"
msgstr "Предложение H"

#: ../../english/template/debian/votebar.wml:118
msgid "Choices"
msgstr "Варианты выбора"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer"
msgstr "Предложивший поправку"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds"
msgstr "Поддержавший поправку"

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text"
msgstr "Текст поправки"

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer A"
msgstr "Предложивший поправку А"

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds A"
msgstr "Поддержавший поправку А"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text A"
msgstr "Текст поправки А"

#: ../../english/template/debian/votebar.wml:139
msgid "Amendment Proposer B"
msgstr "Предложивший поправку Б"

#: ../../english/template/debian/votebar.wml:142
msgid "Amendment Seconds B"
msgstr "Поддержавший поправку Б"

#: ../../english/template/debian/votebar.wml:145
msgid "Amendment Text B"
msgstr "Текст поправки Б"

#: ../../english/template/debian/votebar.wml:148
msgid "Amendment Proposer C"
msgstr "Предложивший поправку В"

#: ../../english/template/debian/votebar.wml:151
msgid "Amendment Seconds C"
msgstr "Поддержавший поправку В"

#: ../../english/template/debian/votebar.wml:154
msgid "Amendment Text C"
msgstr "Текст поправки В"

#: ../../english/template/debian/votebar.wml:157
msgid "Amendments"
msgstr "Поправки"

#: ../../english/template/debian/votebar.wml:160
msgid "Proceedings"
msgstr "Заседания"

#: ../../english/template/debian/votebar.wml:163
msgid "Majority Requirement"
msgstr "Условие большинства"

#: ../../english/template/debian/votebar.wml:166
msgid "Data and Statistics"
msgstr "Данные и статистика"

#: ../../english/template/debian/votebar.wml:169
msgid "Quorum"
msgstr "Кворум"

#: ../../english/template/debian/votebar.wml:172
msgid "Minimum Discussion"
msgstr "Минимальное обсуждение"

#: ../../english/template/debian/votebar.wml:175
msgid "Ballot"
msgstr "Бюллетень"

#: ../../english/template/debian/votebar.wml:178
msgid "Forum"
msgstr "Конференция"

#: ../../english/template/debian/votebar.wml:181
msgid "Outcome"
msgstr "Итог"

#: ../../english/template/debian/votebar.wml:185
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "В&nbsp;ожидании&nbsp;спонсоров"

#: ../../english/template/debian/votebar.wml:188
msgid "In&nbsp;Discussion"
msgstr "Обсуждается"

#: ../../english/template/debian/votebar.wml:191
msgid "Voting&nbsp;Open"
msgstr "Голосование открыто"

#: ../../english/template/debian/votebar.wml:194
msgid "Decided"
msgstr "Принято"

#: ../../english/template/debian/votebar.wml:197
msgid "Withdrawn"
msgstr "Отклонено"

#: ../../english/template/debian/votebar.wml:200
msgid "Other"
msgstr "Другое"

#: ../../english/template/debian/votebar.wml:204
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "На&nbsp;страницу&nbsp;голосований"

#: ../../english/template/debian/votebar.wml:207
msgid "How&nbsp;To"
msgstr "Как"

#: ../../english/template/debian/votebar.wml:210
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "Внести&nbsp;предложение"

#: ../../english/template/debian/votebar.wml:213
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "Внести&nbsp;поправку"

#: ../../english/template/debian/votebar.wml:216
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "Поддержать&nbsp;предложение"

#: ../../english/template/debian/votebar.wml:219
msgid "Read&nbsp;a&nbsp;Result"
msgstr "Узнать&nbsp;результаты"

#: ../../english/template/debian/votebar.wml:222
msgid "Vote"
msgstr "Проголосовать"
