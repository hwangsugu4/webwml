#use wml::debian::cdimage title="Επαλήθευση της αυθεντικότητας των CD του Debian" BARETITLE=true
#use wml::debian::translation-check translation="553c55e0ca1d5d45ffeaab254ac5430bed284238" maintainer="galaxico"

<p>
Οι επίσημες κυκλοφορίες των CD του Debian έρχονται μαζί με υπογεγγραμμένα checksum αρχεία· αναζητήστε τα στις εικόνες στους καταλόγους <code>iso-cd</code>,
<code>jigdo-dvd</code>, <code>iso-hybrid</code> κλπ. Αυτά σας επιτρέπουν να ελέγξετε ότι οι εικόνες που κατεβάσατε είναι σωστές. Πρώτα απ' όλα, το checksum μπορεί να  χρησιμοποιηθεί για να ελέγξετε ότι τα CD δεν έχουν κάποια αλλοίωση/διαφθορά στη διάρκεια της μεταφόρτωσης. Δεύτερον, οι υπογραφές στα αρχεία checksum σας επιτρέπουν να επιβεβαιώσετε ότι τα αρχεία είναι αυτά που έχουν κυκλοφορήσει επίσημα στα CD του Debian / της ομάδας του Debian Live και δεν έχουν αλλοιωθεί.
</p>

<p>
Για να επαληθεύσετε τα περιεχόμενα μιας εικόνας CD, απλά βεβαιωθείτε να χρησιμοποιήσετε το κατάλληλο εργαλείο για checksum. Κρυπτογραφικά ισχυροί αλγόριθμοι checksum (SHA256 and SHA512) είναι διαθέσιμοι για κάθε κυκλοφορία· θα πρέπει να χρησιμοποιήσετε τα εργαλεία  <code>sha256sum</code> ή <code>sha512sum</code> για να δουλέψετε μ' αυτούς.
</p>

<p>
To ensure that the checksums files themselves are correct, use GnuPG to
verify them against the accompanying signature files (e.g.
<code>SHA512SUMS.sign</code>).
The keys used for these signatures are all in the <a
href="https://keyring.debian.org">Debian GPG keyring</a> and the best
way to check them is to use that keyring to validate via the web of
trust.
To make life easier for users, here are the fingerprints for the keys
that have been used for releases in recent years:
</p>

#include "$(ENGLISHDIR)/CD/CD-keys.data"
