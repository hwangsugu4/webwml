msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.1\n"

#: ../../english/index.def:8
msgid "The Universal Operating System"
msgstr "Yleismaailmallinen käyttöjärjestelmä"

#: ../../english/index.def:12
msgid "DebConf is underway!"
msgstr "DebConf on käynnissä!"

#: ../../english/index.def:15
msgid "DebConf Logo"
msgstr "Debonf-tunnus"

#: ../../english/index.def:19
#, fuzzy
#| msgid "DC19 Group Photo"
msgid "DC22 Group Photo"
msgstr "DC19 ryhmäkuva"

#: ../../english/index.def:22
#, fuzzy
#| msgid "DebConf19 Group Photo"
msgid "DebConf22 Group Photo"
msgstr "DebConf19-ryhmäkuva"

#: ../../english/index.def:26
msgid "Debian Reunion Hamburg 2023"
msgstr ""

#: ../../english/index.def:29
#, fuzzy
#| msgid "Group photo of the MiniDebConf in Regensburg 2021"
msgid "Group photo of the Debian Reunion Hamburg 2023"
msgstr "Ryhmäkuva MiniDebConf:ista Regensburgissa 2021"

#: ../../english/index.def:33
msgid "Mini DebConf Regensburg 2021"
msgstr "Mini DebConf tapahtuma Regensburgissa 2021"

#: ../../english/index.def:36
msgid "Group photo of the MiniDebConf in Regensburg 2021"
msgstr "Ryhmäkuva MiniDebConf:ista Regensburgissa 2021"

#: ../../english/index.def:40
msgid "Screenshot Calamares Installer"
msgstr "Kuvakaappaus Calamares-asennusohjelma"

#: ../../english/index.def:43
msgid "Screenshot from the Calamares installer"
msgstr "Kuvakaappaus Calamares-asennusohjelmasta"

#: ../../english/index.def:47 ../../english/index.def:50
msgid "Debian is like a Swiss Army Knife"
msgstr "Debian on kuin sveitsiläinen linkkuveitsi"

#: ../../english/index.def:54
msgid "People have fun with Debian"
msgstr "Ihmisillä on hauskaa Debianin parissa"

#: ../../english/index.def:57
msgid "Debian people at Debconf18 in Hsinchu really having fun"
msgstr ""
"Debian-väkeä todellakin pitämässä hauskaa Debconf18-tapahtumassa Hsinchussa "

#: ../../english/template/debian/navbar.wml:31
msgid "Bits from Debian"
msgstr "Murusia Debianilta"

#: ../../english/template/debian/navbar.wml:31
msgid "Blog"
msgstr "Blogi"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews"
msgstr "Pienuutiset"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews from Debian"
msgstr "Pienuutiset Debianilta"

#: ../../english/template/debian/navbar.wml:33
msgid "Planet"
msgstr "Planeetta"

#: ../../english/template/debian/navbar.wml:33
msgid "The Planet of Debian"
msgstr "Debian-planeetta"
