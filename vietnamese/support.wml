#use wml::debian::template title="Hỗ trợ"
#use wml::debian::toc
#use wml::debian::translation-check translation="0d2b6f2d18fa25263c9500a1977d01a576281ca0" maintainer="Tuấn Nguyễn Anh,Trần Ngọc Quân"

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

Debian và các hỗ trợ cho nó được thực hiện bởi cộng đồng của những người tình nguyện.

Nếu sự hỗ trợ từ cộng đồng không đáp ứng được nhu cầu của bạn, bạn nên
đọc <a href="doc/">tài liệu</a> của chúng tôi hoặc
thuê <a href="consultants/">tư vấn</a>.

<toc-display />

<toc-add-entry name="irc">Trợ giúp thời gian thực trực tuyến sử dụng IRC</toc-add-entry>

<p><a href="http://www.irchelp.org/">IRC (Internet Relay Chat)</a> là một cách
trò chuyện với mọi người trên khắp thế giới trong thời gian thực.
Các kênh IRC dành cho Debian có thể được tìm thấy tại
<a href="https://www.oftc.net/">OFTC</a>.</p>

<p>Để kết nối, bạn cần một trình khách IRC. Một trong số những trình khách phổ biến nhất là
<a href="https://packages.debian.org/stable/net/hexchat">HexChat</a>,
<a href="https://packages.debian.org/stable/net/ircii">ircII</a>,
<a href="https://packages.debian.org/stable/net/irssi">irssi</a>,
<a href="https://packages.debian.org/stable/net/epic5">epic5</a> và
<a href="https://packages.debian.org/stable/net/kvirc">KVIrc</a>,
tất cả đều được đóng gói cho
Debian. OFTC còn cung cấp một giao diện web <a href="https://www.oftc.net/WebChat/">WebChat</a>
cho phép người dùng có thể kết nối với IRC bằng một trình duyệt mà không cần
cài đặt bất trình khách nội bộ nào.</p>

<p>Một khi trình khách đã được cài đặt, bạn cần báo cho nó để kết nối
đến máy chủ. Đối với hầu hết các trình khách, bạn chỉ cần gõ:</p>

<pre>
/server irc.debian.org
</pre>

<p>Trong một số máy khách (như irssi) bạn sẽ cần phải thay cách gõ như sau:</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
# <p>Once you are connected, join channel <code>#debian-foo</code> by typing</p>
# <pre>/join #debian</pre>
# for support in your language.
# <p>For support in English, read on</p>

<p>Một khi bạn đã kết nối được, gia nhập vào kênh <code>#debian</code> bằng cách gõ</p>

<pre>
/join #debian
</pre>

<p>Lưu ý: các trình khách như HexChat thường có giao diện người dùng khác biệt
khi gia nhập vào máy chủ/kênh.</p>

<p>Đến lúc này, bạn sẽ cảm thấy mình đã là một thành viên trong cộng đồng
<code>#debian</code> thân thiện. Các câu trả hỏi về Debian của bạn luôn được hoan nghênh.
Bạn cũng có thể tìm thấy các kênh về những câu hỏi thường gặp tại
<url "https://wiki.debian.org/DebianIRC" />.</p>


<p>Ngoài ra, còn có một số mạng IRC khác nữa
mà bạn có thể nói chuyện về Debian.</p>


<toc-add-entry name="mail_lists" href="MailingLists/">Các bó thư</toc-add-entry>

<p>Debian được phát triển từ những đóng góp trên toàn
thế giới. Do đó, thư điện tử là phương thức được ưa chuộng để thảo luận nhiều vấn đề.
Phần lớn các cuộc đối thoại giữa người phát triển Debian và người sử dụng được quản lý
thông qua các bó thư.</p>

<p>Có một vài danh sách thư được công bố một cách rộng rãi. Để có thêm thông tin,
vui lòng vào trang <a href="MailingLists/">Các bó thư Debian</a>.</p>

# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.
<p>
Đối với người dùng sử dụng tiếng Việt, hãy liên hệ
<a href="https://lists.debian.org/debian-user-vietnamese/">Bó thư cho người Việt dùng Debian</a>.
Ngoài ra bạn có thể liên lạc với nhóm Việt hóa Debian thông qua
<a href="https://lists.debian.org/debian-l10n-vietnamese/">bó thư của nhóm</a>.
Tuy đây chỉ là nhóm dịch thuật nhưng cũng có khá nhiều người có kinh nghiệm
sử dụng Debian nói riêng và họ hệ điều hành Gnu/Linux nói chung.
</p>

<p>
Đối với người dùng sử dụng tiếng Anh, hãy liên hệ
<a href="https://lists.debian.org/debian-user/">Bó thư cho người dùng Debian</a>.
</p>

<p>
Đối với người dùng sử dụng những ngôn ngữ khác, hãy kiểm tra
<a href="https://lists.debian.org/users.html">danh mục bó thư
cho người dùng</a>.
</p>

<p>Tất nhiên là còn có nhiều bó thư khác, phục vụ cho một vài khía cạnh
của hệ sinh thái Linux, và chúng không chuyên biệt cho Debian. Hãy sử dụng
công cụ tìm kiếm ưa thích của bạn để tìm được một bó thư phù hợp nhất cho mục đích của bạn.</p>


<toc-add-entry name="usenet">Nhóm tin tức Usenet</toc-add-entry>

<p>Rất nhiều <a href="#mail_lists">bó thư</a> của chúng tôi có thể được mở như là
những nhóm tin tức, trong cấu trúc <kbd>linux.debian.*</kbd>. Nó cũng có thể được
thực thi bằng việc sử dụng giao diện web, ví dụ như
<a href="https://groups.google.com/forum/">Google Groups</a>.</p>


<toc-add-entry name="web">Các trang thông tin điện tử</toc-add-entry>

<h3>Các diễn đàn</h3>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
#
# <p><a href="https://forums.debian.net">Debian User Forums</a> is a web portal
# on which you can use the English language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>

<p><a href="https://forums.debian.net">Diễn đàn của người dùng Debian</a> là những cổng thông tin điện tử
nơi bạn có thể thảo luận những chủ đề về Debian, đặt ra câu hỏi về Debian và nhận được
câu trả lời từ những người dùng khác.</p>


<toc-add-entry name="maintainers">Liên lạc với người bảo trì gói</toc-add-entry>

<p>Có hai cách để liên lạc với người bảo trì gói. Nếu như bạn cần
liên lạc với người bảo trì vì một lỗi nào đó, đơn giản bạn chỉ cần gửi một báo cáo lỗi
(xem phần Hệ thống theo dõi lỗi bên dưới). Người bảo trì sẽ nhận được bản sao
của bản báo cáo lỗi đó.</p>

<p>Nếu như chỉ đơn thuần là bạn muốn nói chuyện với người bảo trì, thì bạn có thể dùng
một biệt danh thư đặc biệt được cài đặt cho mỗi gói. Bất cứ thư nào gửi đến
&lt;<em>tên gói</em>&gt;@packages.debian.org sẽ được chuyển tiếp đến
người bảo trì có trách nhiệm đối với gói đó.</p>


<toc-add-entry name="bts" href="Bugs/">Hệ thống theo dõi lỗi</toc-add-entry>

<p>Bản phân phối Debian có một hệ thống theo dõi lỗi
vạch ra chi tiết những lỗi được báo cáo bởi người dùng và người phát triển. Mỗi lỗi sẽ
được gán cho một con số, và được lưu trữ cho đến khi nó được đánh dấu là
đã xử lý.</p>

<p>Để báo cáo một lỗi, bạn có thể đọc các trang về lỗi được liệt kê ở dưới; chúng tôi khuyến nghị
bạn dùng gói <q>reportbug</q> Debian để tự động báo cáo một lỗi.</p>

<p>Thông tin về việc báo lỗi, xem các lỗi hiện tại, và
xem hệ thống báo lỗi một các tổng quát, các bạn có thể truy cập
<a href="Bugs/">các trang thông tin điện tử hệ thống theo dõi lỗi</a>.</p>


<toc-add-entry name="doc" href="doc/">Tài liệu</toc-add-entry>

<p>Một phần quan trọng của bất kỳ hệ điều hành nào là tài liệu,
hướng dẫn kỹ thuật mô tả việc vận hành và sử dụng các chương trình. Là một phần
trong nỗ lực tạo ra hệ điều hành tự do chất lượng cao, Dự án Debian
đang cố gắng hết sức để cung cấp cho tất cả người dùng tài liệu
thích hợp ở dạng dễ truy cập.</p>

<p>Xem <a href="doc/">trang tài liệu</a> để có danh sách các
hướng dẫn sử dụng Debian và các tài liệu khác, bao gồm Hướng dẫn cài đặt,
Debian FAQ, và các hướng dẫn cho người dùng và nhà phát triển khác nữa.</p>


<toc-add-entry name="consultants" href="consultants/">Tư vấn</toc-add-entry>

<p>Debian là phần mềm tự do và cung cấp cách trợ giúp miễn phí thông qua các bó thư. Một số
người không có thời gian hoặc có nhu cầu đặc biệt sẵn sàng
thuê một ai đó để duy trì hoặc bổ sung các chức năng bổ trợ cho hệ thống Debian
của họ. Truy cập <a href="consultants/">trang tư vấn</a> để xem danh sách
công ty/cá nhân.</p>


<toc-add-entry name="release" href="releases/stable/">Những vấn đề đã biết</toc-add-entry>

<p>Những hạn chế và những vấn đề nghiêm trọng của bản phân phối ổn định
(nếu có) được mô tả trên <a href="releases/stable/">các trang phát hành</a>.</p>

<p>Hãy chú ý đến <a href="releases/stable/releasenotes">các ghi chú
phát hành</a> và <a href="releases/stable/errata">các đính chính</a>.</p>
