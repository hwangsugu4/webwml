#use wml::debian::template title="Debian &ldquo;wheezy&rdquo; Release Information"
#include "$(ENGLISHDIR)/releases/info"



<p>Debian <current_release_wheezy> was
released <a href="$(HOME)/News/<current_release_newsurl_wheezy/>"><current_release_date_wheezy></a>.
<ifneq "7.0" "<current_release>"
  "Debian 7.0 was initially released on <:=spokendate('2013-05-04'):>."
/>
The release included many major
changes, described in 
our <a href="$(HOME)/News/2013/20130504">press release</a> and 
the <a href="releasenotes">Release Notes</a>.</p>

<p><strong>Debian 7 has been superseded by
<a href="../jessie/">Debian 8 (<q>jessie</q>)</a>.
# Security updates have been discontinued as of <:=spokendate('XXXXXXXXXXX'):>.
</strong></p>

<p><strong>Wheezy also had benefited from Long Term Support (LTS) until
the end of May 2018. The LTS was limited to i386, amd64, armel and armhf.
For more information, please refer to the <a
href="https://wiki.debian.org/LTS">LTS section of the Debian Wiki</a>.
</strong></p>

<p>To obtain and install Debian, see
the installation information page and the
Installation Guide. To upgrade from an older
Debian release, see the instructions in the
<a href="releasenotes">Release Notes</a>.</p>

<p>The following computer architectures are supported in this release:</p>

<ul>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
<li><a href="../../ports/sparc/">SPARC</a>
<li><a href="../../ports/kfreebsd-amd64/">kFreeBSD 64-bit PC (amd64)</a>
<li><a href="../../ports/ia64/">Intel Itanium IA-64</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/kfreebsd-i386/">kFreeBSD 32-bit PC (i386)</a>
<li><a href="../../ports/s390/">IBM S/390</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/s390x/">IBM System z</a>
</ul>

<p>Contrary to our wishes, there may be some problems that exist in the
release, even though it is declared <em>stable</em>. We've made
<a href="errata">a list of the major known problems</a>, and you can always
report other issues to us.</p>
