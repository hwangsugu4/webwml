#use wml::debian::translation-check translation="02a4456945ea359db9609e01c3c84459814434c2" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de l'installateur Debian Bookworm RC2</define-tag>
<define-tag release_date>2023-04-28</define-tag>
#use wml::debian::news

<p>
L'<a href="https://wiki.debian.org/DebianInstaller/Team">équipe</a> du
programme d'installation de Debian a le plaisir d'annoncer la parution
de la deuxième version candidate de l’installateur pour Debian 12 <q>Bookworm</q>.
</p>

<h2>Améliorations dans cette version de l'installateur</h2>

<ul>
  <li>apt-setup :
    <ul>
      <li>activation de &lt;codename&gt;-updates lors de l'installation de testing
        et pas seulement lors de l'installation de (old)stable (<a href="https://bugs.debian.org/991947">nº 991947</a>).</li>
    </ul>
  </li>
  <li>argon2 :
    <ul>
      <li>restauration de la prise en charge du threading dans l'installateur
        (<a href="https://bugs.debian.org/1034696">nº 1034696</a>).</li>
    </ul>
  </li>
  <li>base-installer :
    <ul>
      <li>installation de zstd en même temps qu'initramfs-tools, comme busybox.</li>
    </ul>
  </li>
  <li>choose-mirror :
    <ul>
      <li>mise à jour de Mirrors.masterlist.</li>
    </ul>
  </li>
  <li>cryptsetup :
    <ul>
      <li>amélioration de la prise en charge des systèmes avec peu de mémoire 
        (<a href="https://bugs.debian.org/1028250">nº 1028250</a>) : vérification
        de la mémoire physique disponible aussi dans l'évaluation de PBKDF, et
        utilisation de seulement la moitié de la mémoire disponible détectée
        sur les systèmes sans partition d'échange.</li>
    </ul>
  </li>
  <li>debian-cd :
    <ul>
      <li>inclusion de zstd en même temps qu'initramfs-tools, comme busybox.</li>
    </ul>
  </li>
  <li>debian-installer :
    <ul>
      <li>correction du chargement de la fonte de grub, assurant l'affichage
        du bel écran graphique d'accueil avec le mode Secure Boot mode également
        (<a href="https://bugs.debian.org/1034535">nº 1034535</a>).</li>
    </ul>
  </li>
  <li>grub-installer :
    <ul>
      <li>installation de shim-signed sur i386 et arm64 ainsi que sur amd64
        lors de l'installation des paquets grub-efi-*-signed ;</li>
      <li>si un autre système d'exploitation est détecté, vérification avec
        l'utilisateur (avec une priorité faible) et réactivation d'os-prober.</li>
    </ul>
  </li>
  <li>grub2 :
    <ul>
      <li>ajout de la logique de debconf pour GRUB_DISABLE_OS_PROBER pour
        faciliter le contrôle, particulièrement utile pour l'installateur
        (<a href="https://bugs.debian.org/1031594">nº 1031594</a>,
        <a href="https://bugs.debian.org/1012865">nº 1012865</a>, <a href="https://bugs.debian.org/1025698">nº 1025698</a>) ;</li>
      <li>ajout de luks2 aux images grub-efi signées (<a href="https://bugs.debian.org/1001248">nº 1001248</a>) ;</li>
      <li>correction du sondage des périphériques LUKS2 (<a href="https://bugs.debian.org/1028301">nº 1028301</a>) ;</li>
      <li>correction de Secure Boot sur arm64 (<a href="https://bugs.debian.org/1033657">nº 1033657</a>) ;</li>
      <li>pas d'avertissement au sujet de l'os-prober s'il n'est pas installé
        (<a href="https://bugs.debian.org/1020769">nº 1020769</a>) ;</li>
      <li>correction d'un problème où le renommage d'un volume logique pouvait
        faire échouer l'amorçage par grub (<a href="https://bugs.debian.org/987008">nº 987008</a>).</li>
    </ul>
  </li>
  <li>live-installer :
    <ul>
      <li>suppression des paquets live avant le démontage du cdrom, évitant une
        longue attente à la fin du processus d'installation.</li>
    </ul>
  </li>
  <li>partman-efi :
    <ul>
      <li>correction de la détection des systèmes avec amorçage par le BIOS,
        corrigeant au moins le cas d'utilisation « Assisté — utiliser tout un
        disque avec LVM » sur les systèmes UEFI (<a href="https://bugs.debian.org/834373">nº 834373</a>,
        <a href="https://bugs.debian.org/1033913">nº 1033913</a>).</li>
    </ul>
  </li>
  <li>preseed :
    <ul>
      <li>restauration de la prise en charge de l'alias « hostname » dans la
        ligne de commande du noyau
        (<a href="https://bugs.debian.org/1031643">nº 1031643</a>, <a href="https://bugs.debian.org/1034062">nº 1034062</a>) ;</li>
      <li>passer auto-install/defaultroot à Bookworm.</li>
    </ul>
  </li>
</ul>


<h2>Modifications de la prise en charge matérielle</h2>

<ul>
  <li>flash-kernel :
    <ul>
      <li>ajustement du script d'amorçage du XO-1.75 d'OLPC ;</li>
      <li>ajout des Miix 630 et Yoga C630 de Lenovo ;</li>
      <li>ajout de la carte StarFive VisionFive ;</li>
      <li>ajout des cartes D1 SoC ;</li>
      <li>ajout d'entrées « factices » relatives à QEMU ;</li>
      <li>ajout de la carte A20-OLinuXino_MICRO-eMMC (<a href="https://bugs.debian.org/1019881">nº 1019881</a>) ;</li>
      <li>ajout du ThinkPad X13s de Lenovo ;</li>
      <li>ajout de la catre Colibri iMX6ULL eMMC ;</li>
      <li>ajout du Raspberry Pi 3 Modèle B Plus Rev 1.3.</li>
    </ul>
  </li>
  <li>linux :
    <ul>
      <li>udeb : ajout de intel_lpss* (optionnel) à kernel-image (<a href="https://bugs.debian.org/1032136">nº 1032136</a>).
        De nombreux ordinateurs portables ont un touchpad accessible avec I2C,
        seulement visible si LPSS est disponible dans l'installateur.</li>
    </ul>
  </li>
</ul>


<h2>État de la localisation</h2>

<ul>
  <li>78 langues sont prises en charge dans cette version.</li>
  <li>La traduction est complète pour 41 de ces langues.</li>
</ul>


<h2>Problèmes connus dans cette version</h2>

<p>
Veuillez consulter les <a href="$(DEVEL)/debian-installer/errata">errata</a>
pour plus de détails et une liste complète des problèmes connus.
</p>


<h2>Retours d'expérience pour cette version</h2>

<p>
Nous avons besoin de votre aide pour trouver des bogues et améliorer encore
l'installateur, merci de l'essayer. Les CD, les autres médias d'installation,
et tout ce dont vous pouvez avoir besoin sont disponibles sur notre
<a href="$(DEVEL)/debian-installer">site web</a>.
</p>


<h2>Remerciements</h2>

<p>
L'équipe du programme d'installation Debian remercie toutes les personnes ayant
pris part à cette publication.
</p>
