#use wml::debian::template title="Säkerhetsinformation" GEN_TIME="yes" MAINPAGE="true"
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="452896bf2af61f852728d258a34c0609dd632373"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#keeping-secure">Att hålla ditt Debiansystem säkert</a></li>
<li><a href="#DSAS">Senaste bulletinerna</a></li>
<li><a href="#infos">Källor med säkerhetsinformation</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian ser väldigt allvarligt på säkerhet. 
Vi hanterar alla säkerhetsproblem vi görs uppmärksamma på och ser till att de
korrigeras inom en rimlig tidsram.</p>
</aside>

<p>
Erfarenhet visar att <q>säkerhet genom otydlighet</q> aldrig fungerar. Därför
tillåter publikt offentliggörande snabbare och bättre lösningar av 
säkerhetsproblem. I det avseendet tar den här sidan upp Debians status angående
olika kända säkerhetshål, som potentiellt kan påverka Debianoperativsystemet.
</p>

<p>
Debianprojektet koordinerar många säkerhetsbulletiner med andra
mjukvaruutvecklare, och som ett resultat av detta publiceras dessa bulletiner
samma dag som sårbarheten är publik.
För att få de senaste säkerhetsbulletinerna, vänligen prenumerera på sändlistan
<a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.
</p>
 
<p>
Debian deltar även i projekt för standardisering av säkerhetsinformation:
</p>

<ul>
   <li>
      <a href="#DSAS">Debians säkerhetsbulletiner</a> är <a href="cve-compatibility">CVE-kompatibla</a> (se <a href="crossreferences">korsreferenser</a>).
   </li>
   <li>
      Debian <a href="oval/">publicerar</a> sin säkerhetsinformation mha <a href="https://github.com/CISecurity/OVALRepo">Open Vulnerability Assessment Language (OVAL)</a>
   </li>
</ul>


<h2><a id="keeping-secure">Håll ditt Debiansystem säkert</a></h2>

<p>
Paketet <a href="https://packages.debian.org/stable/admin/unattended-upgrades">unattended-upgrades</a> 
kan installeras för att hålla datorn aktuell med de senaste säkerhetsuppdateringarna
(och andra uppdateringar) automatiskt.

<a href="https://wiki.debian.org/UnattendedUpgrades">wikisidan</a> har
ytterligare detaljerad information om hur du kan ställa in 
<tt>unattended-upgrades</tt> för hand.


<p>
För mer information om säkerhetsproblem i Debian, vänligen se vår FAQ och vår
dokumentation:
</p>


<p style="text-align:center"><button type="button"><span 
class="fas fa-book-open fa-2x"></span> <a href="faq">Security FAQ</a></button> <button 
type="button"><span class="fas fa-book-open fa-2x"></span> <a 
href="../doc/user-manuals#securing">Securing Debian</a></button></p>

<aside class="light">
  <span class="fa fa-rss fa-5x"></span>
</aside>

<h2><a id="DSAS">Senaste säkerhetsbulletinerna</a> <a class="rss_logo" style="float: none;" href="dsa">RSS</a></h2>



<p>Detta ör de senaste säkerhetsbulletinerna (Debian Security Advisories - DSA) som postas på sändlistan <a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.
<br><b>T</b> är länken till <a href="https://security-tracker.debian.org/tracker">Debians säkerhetsspårare</a>, DSA-numret länkar tillkännagivandemailet.


<p>
#include "$(ENGLISHDIR)/security/dsa.list"
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debians säkerhetsbulletiner (endast rubriker)" href="dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debians säkerhetsbulletiner (sammanfattning)" href="dsa-long">
:#rss#}

<h2><a id="infos">Källor med säkerhetsinformation</a></h2>
#include "security-sources.inc"
