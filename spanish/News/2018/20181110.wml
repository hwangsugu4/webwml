#use wml::debian::translation-check translation="ea6a13e28f99e15f67ccfd6b74b4cc4bb185fdbd"
<define-tag pagetitle>Debian 9 actualizado: publicada la versión 9.6</define-tag>
<define-tag release_date>2018-11-10</define-tag>
#use wml::debian::news

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.6</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la sexta actualización de su
distribución «estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction accerciser "Corrige el acceso a elementos sin un compositor; corrige la consola Python; añade dependencia con python3-xlib, que faltaba">
<correction apache2 "mod_http2: corrige denegación de servicio por uso de todos los trabajadores disponibles [CVE-2018-1333] y por envío de SETTINGS continuos [CVE-2018-11763]; mod_proxy_fcgi: corrige fallo de segmentación">
<correction base-files "Actualiza /etc/debian_version para esta versión">
<correction brltty "Corrige autenticación con polkit">
<correction canna "Corrige conflicto de ficheros entre canna-dbgsym y canna-utils-dbgsym">
<correction cargo "Nuevo paquete para soportar la compilación de Firefox ESR60">
<correction clamav "Nueva versión del proyecto original; corrige vulnerabilidad de desbordamiento de entero HWP que provoca bucle infinito [CVE-2018-0360]; corrige problema de comprobación de longitud de objeto PDF, más tiempo de lo razonable para analizar sintácticamente un fichero relativamente pequeño [CVE-2018-0361]; nueva versión del proyecto original; corrige problema de denegación de servicio [CVE-2018-15378]; corrige bucle infinito en dpkg-reconfigure">
<correction confuse "Corrige una lectura fuera de límites en trim_whitespace [CVE-2018-14447]">
<correction debian-installer "Actualizado para la ABI -8 del kernel">
<correction debian-installer-netboot-images "Recompilado para esta versión">
<correction dnsmasq "trust-anchors.conf: incluye el ancla de confianza («trust anchor») de DNS más reciente KSK-2017">
<correction dom4j "Corrige ataque de inyección de XML [CVE-2018-1000632]; compila con fuente/objetivo («target») 1.5 para corregir un problema de compilación con String.format">
<correction dpdk "Nueva versión «estable» del proyecto original">
<correction dropbear "Corrige vulnerabilidad de enumeración de usuarios [CVE-2018-15599]">
<correction easytag "Corrige corrupción de OGG">
<correction enigmail "Añade compatibilidad con versiones más recientes de Thunderbird">
<correction espeakup "espeakup.service: carga speakup_soft automáticamente en el arranque del daemon">
<correction fastforward "Corrige fallos de segmentación en arquitecturas de 64 bits">
<correction firetray "Añade compatibilidad con versiones más recientes de Thunderbird">
<correction firmware-nonfree "Corrige problemas de seguridad en firmware wifi de Broadcom [CVE-2016-0801 CVE-2017-0561 CVE-2017-9417 CVE-2017-13077 CVE-2017-13078 CVE-2017-13079 CVE-2017-13080 CVE-2017-13081]; añade de nuevo paquetes de transición para firmware-{adi,ralink}">
<correction fofix-dfsg "Corrige error en el arranque">
<correction fuse "Aprueba («whitelist») autofs y FAT como sistemas de archivos de puntos de montaje válidos">
<correction ganeti "Verifica correctamente certificados SSL durante VM export; firma los certificados generados utilizando SHA256 en lugar de SHA1; hace autocargables las configuraciones de autocompletado de bash">
<correction globus-gsi-credential "Corrige problema con proxy voms y openssl 1.1">
<correction gnupg2 "Correcciones de seguridad; adaptada funcionalidad requerida por el nuevo enigmail">
<correction gnutls28 "Corrige problemas de seguridad [CVE-2018-10844 CVE-2018-10845]">
<correction gphoto2-cffi "Hace que python3-gphoto2cffi vuelva a funcionar">
<correction grub2 "grub-mknetdir: añade soporte para ARM64 EFI; cambia el método de calibrado por omisión del TSC a pmtimer en sistemas EFI">
<correction hdparm "Solo habilita APM en los discos que lo anuncian">
<correction https-everywhere "Adapta nueva versión del proyecto original, para compatibilidad con Firefox ESR 60">
<correction i3-wm "Corrige caída tras el reinicio cuando se utilizan marcas («marks»)">
<correction iipimage "Corrige configuración de Apache">
<correction jhead "Corrige problemas de seguridad [CVE-2018-17088 CVE-2018-16554]">
<correction lastpass-cli "Adapta los pins del certificado incluidos en el código de lastpass-cli 1.3.1 para reflejar los cambios en el servicio Lastpass.com">
<correction ldap2zone "Corrige bucle sin fin al comprobar el número de serie de la zona">
<correction libcgroup "Corrige ficheros de log accesibles (y modificables) universalmente [CVE-2018-14348]">
<correction libclamunrar "Nueva versión del proyecto original">
<correction libdap "Corrige contenidos de libdap-doc">
<correction libdatetime-timezone-perl "Actualiza los datos incluidos">
<correction libgd2 "Bmp: comprueba el valor devuelto en gdImageBmpPtr [CVE-2018-1000222]; corrige potencial bucle infinito en gdImageCreateFromGifCtx [CVE-2018-5711]">
<correction libmail-deliverystatus-bounceparser-perl "Elimina ejemplos no distribuibles de correo no deseado y de virus">
<correction libmspack "Corrige escritura fuera de límites [CVE-2018-18584] y aceptación de nombres de fichero <q>en blanco</q> [CVE-2018-18585]">
<correction libopenmpt "Corrige <q>up11: lectura fuera de límites al cargar ficheros IT / MO3 con muchos bucles de patrones</q> [CVE-2018-10017]">
<correction libseccomp "Añade soporte para llamadas al sistema de Linux 4.9: preadv2, pwritev2, pkey_mprotect, pkey_alloc y pkey_free; añade soporte para statx">
<correction libtirpc "rendezvous_request: comprueba el valor devuelto por makefd_xprt [CVE-2018-14622]">
<correction libx11 "Corrige varios problemas de seguridad [CVE-2018-14598 CVE-2018-14599 CVE-2018-14600]">
<correction libxcursor "Corrige una denegación de servicio o, potencialmente, ejecución de código por medio de un desbordamiento de memoria dinámica («heap») por exceso de un byte [CVE-2015-9262]">
<correction libxml-stream-perl "Proporciona una ruta de CA por omisión">
<correction libxml-structured-perl "Añade dependencia en compilación y en ejecución con libxml-parser-perl, que faltaba">
<correction linux "Xen: corrige regresión en arranque en dominios PV; xen-netfront: corrige regresiones; ext4: corrige falsos negativos *y* falsos positivos en ext4_check_descriptors(); udeb: añade virtio_console a virtio-modules; cdc_ncm: evita el relleno más allá del fin de skb; revierte <q>sit: carga de nuevo iphdr en ipip6_rcv</q>; nueva versión del proyecto original">
<correction lxcfs "Revierte la virtualización del tiempo de actividad, corrigiendo las horas de arranque de los procesos">
<correction magicmaze "Ahora que ttf-isabella es un paquete virtual, depende de fonts-isabella">
<correction mailman "Corrige vulnerabilidad de inyección de texto arbitrario en los CGI de Mailman [CVE-2018-13796]">
<correction multipath-tools "Evita interbloqueo en disparadores de udev">
<correction nagstamon "Aborda problema de autenticación Basic en IcingaWeb2">
<correction network-manager "libnm: corrige acceso a propiedades enabled y metered; corrige escritura fuera de límites en la memoria dinámica («heap») en la gestión de la opción dhcpv6 [CVE-2018-15688] y varios otros problemas en la extensión («plugin») dhcp=internal, basada en sd-network">
<correction network-manager-applet "libnma/pygobject: libnma/NMA debe usar libnm/NM en lugar de usar bibliotecas heredadas">
<correction ola "Corrige error tipográfico en /etc/init.d/rdm_test_server; corrige nombre de fichero para jquery en ficheros de HTML estático del servidor de pruebas de rdm">
<correction opensc "Corrige recursión infinita y varias lecturas y escrituras fuera de límites [CVE-2018-16391 CVE-2018-16392 CVE-2018-16393 CVE-2018-16418 CVE-2018-16419 CVE-2018-16420 CVE-2018-16421 CVE-2018-16422 CVE-2018-16423 CVE-2018-16424 CVE-2018-16425 CVE-2018-16426 CVE-2018-16427]">
<correction pkgsel "Instala nuevas dependencias cuando se selecciona safe-upgrade (seleccionado por omisión)">
<correction publicsuffix "Actualiza los datos incluidos">
<correction python-django "Por omisión soporta Spatialite &gt;= 4.2">
<correction python-imaplib2 "Instala el módulo de Python 3 correcto; no usa TIMEOUT_MAX">
<correction rustc "Habilita la compilación en más arquitecturas: arm64, armel, armhf, i386, ppc64el, s390x">
<correction sddm "Respeta los grupos complementarios ambientales de PAM; añade gestión de utmp/wtmp/btmp, que faltaba">
<correction serf "Corrige desreferencia de puntero NULL">
<correction soundconverter "Corrige el valor configurado de vbr para opus">
<correction spamassassin "Nueva versión del proyecto original; corrige denegación de servicio [CVE-2017-15705], ejecución de código remoto [CVE-2018-11780], inyección de código [CVE-2018-11781] y uso no seguro de <q>.</q> en @INC [CVE-2016-1238]; corrige gestión del servicio spamd en actualizaciones del paquete">
<correction spice-gtk "Corrige desbordamiento de matriz flexible («flexible array») [CVE-2018-10873]">
<correction sqlcipher "Evita caída al abrir un fichero">
<correction subversion "Corrige una regresión introducida por las correcciones para colisiones SHA1, por la que los commits fallarían indebidamente con un error <q>Filesystem is corrupt</q> («El sistema de archivos está corrupto») si la longitud de la diferencia («delta») es múltiplo de 16K">
<correction systemd "networkd: manager_connect_bus() no falla aunque dbus no esté activo todavía; dhcp6: se asegura de que tenemos espacio suficiente para la cabecera de la opción DHCP6 [CVE-2018-15688]">
<correction systraq "Invierte la lógica para terminar sin errores en caso de que no exista /e/s/Makefile">
<correction tomcat-native "Corrige problema de respondedor OSCP que hacía posible que los usuarios se autenticaran con certificados revocados al utilizar autenticación TLS recíproca («mutual TLS») [CVE-2018-8019 CVE-2018-8020]">
<correction tor "Cambios de autoridad de directorio: retira la autoridad de puente <q>Bifroest</q>, en favor de <q>Serge</q>; añade una dirección IPv6 para la autoridad de directorio <q>dannenberg</q>">
<correction tzdata "Nueva versión del proyecto original">
<correction ublock-origin "Adaptación de nueva versión del proyecto original, para compatibilidad con Firefox ESR 60">
<correction unbound "Corrige vulnerabilidad en el tratamiento de registros NSEC sintetizados comodín [CVE-2017-15105]">
<correction vagrant "Soporta VirtualBox 5.2">
<correction vmtk "python-vmtk: añade la dependencia con python-vtk6, que faltaba">
<correction wesnoth-1.12 "No permite la carga de bytecode lua por medio de load/dofile [CVE-2018-1999023]">
<correction wpa "Ignora datos EAPOL-Key que estén cifrados y no autenticados [CVE-2018-14526]">
<correction x11vnc "Corrige dos desbordamientos de memoria">
<correction xapian-core "Corrige fallo en el backend glass con cursores de larga duración sobre una tabla en una WritableDatabase que podía dar lugar al lanzamiento, de forma incorrecta, de un DatabaseCorruptError cuando, en realidad, la base de datos estaba bien">
<correction xmotd "Evita caída con indicadores para endurecimiento («hardening flags»)">
<correction xorg-server "GLX: no usa la configuración de sRGB para RGBA visual de 32 bits - corrige varios problemas de mezcla con kwin y Mesa &gt;= 18.0 (es decir, Mesa de stretch-backports)">
<correction zutils "Corrige un desbordamiento de memoria en zcat [CVE-2018-1000637]">
</table>

<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2017 4074 imagemagick>
<dsa 2018 4103 chromium-browser>
<dsa 2018 4182 chromium-browser>
<dsa 2018 4237 chromium-browser>
<dsa 2018 4242 ruby-sprockets>
<dsa 2018 4243 cups>
<dsa 2018 4244 thunderbird>
<dsa 2018 4245 imagemagick>
<dsa 2018 4246 mailman>
<dsa 2018 4247 ruby-rack-protection>
<dsa 2018 4248 blender>
<dsa 2018 4249 ffmpeg>
<dsa 2018 4250 wordpress>
<dsa 2018 4251 vlc>
<dsa 2018 4252 znc>
<dsa 2018 4253 network-manager-vpnc>
<dsa 2018 4254 slurm-llnl>
<dsa 2018 4256 chromium-browser>
<dsa 2018 4257 fuse>
<dsa 2018 4258 ffmpeg>
<dsa 2018 4260 libmspack>
<dsa 2018 4261 vim-syntastic>
<dsa 2018 4262 symfony>
<dsa 2018 4263 cgit>
<dsa 2018 4264 python-django>
<dsa 2018 4265 xml-security-c>
<dsa 2018 4266 linux>
<dsa 2018 4267 kamailio>
<dsa 2018 4268 openjdk-8>
<dsa 2018 4269 postgresql-9.6>
<dsa 2018 4270 gdm3>
<dsa 2018 4271 samba>
<dsa 2018 4272 linux>
<dsa 2018 4273 intel-microcode>
<dsa 2018 4274 xen>
<dsa 2018 4275 keystone>
<dsa 2018 4276 php-horde-image>
<dsa 2018 4277 mutt>
<dsa 2018 4278 jetty9>
<dsa 2018 4279 linux>
<dsa 2018 4279 linux-latest>
<dsa 2018 4280 openssh>
<dsa 2018 4281 tomcat8>
<dsa 2018 4282 trafficserver>
<dsa 2018 4283 ruby-json-jwt>
<dsa 2018 4284 lcms2>
<dsa 2018 4285 sympa>
<dsa 2018 4286 curl>
<dsa 2018 4287 firefox-esr>
<dsa 2018 4288 ghostscript>
<dsa 2018 4289 chromium-browser>
<dsa 2018 4290 libextractor>
<dsa 2018 4291 mgetty>
<dsa 2018 4292 kamailio>
<dsa 2018 4293 discount>
<dsa 2018 4294 ghostscript>
<dsa 2018 4295 thunderbird>
<dsa 2018 4296 mbedtls>
<dsa 2018 4297 chromium-browser>
<dsa 2018 4298 hylafax>
<dsa 2018 4299 texlive-bin>
<dsa 2018 4300 libarchive-zip-perl>
<dsa 2018 4301 mediawiki>
<dsa 2018 4302 openafs>
<dsa 2018 4303 okular>
<dsa 2018 4304 firefox-esr>
<dsa 2018 4305 strongswan>
<dsa 2018 4306 python2.7>
<dsa 2018 4307 python3.5>
<dsa 2018 4308 linux>
<dsa 2018 4309 strongswan>
<dsa 2018 4310 firefox-esr>
<dsa 2018 4311 git>
<dsa 2018 4312 tinc>
<dsa 2018 4313 linux>
<dsa 2018 4314 net-snmp>
<dsa 2018 4315 wireshark>
<dsa 2018 4316 imagemagick>
<dsa 2018 4317 otrs2>
<dsa 2018 4318 moin>
<dsa 2018 4319 spice>
<dsa 2018 4320 asterisk>
<dsa 2018 4321 graphicsmagick>
<dsa 2018 4322 libssh>
<dsa 2018 4323 drupal7>
<dsa 2018 4324 firefox-esr>
<dsa 2018 4325 mosquitto>
<dsa 2018 4326 openjdk-8>
<dsa 2018 4327 thunderbird>
<dsa 2018 4328 xorg-server>
<dsa 2018 4329 teeworlds>
<dsa 2018 4331 curl>
</table>


<h2>Paquetes eliminados</h2>

<p>Se han eliminado los paquetes listados a continuación por circunstancias ajenas a nosotros:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction adblock-plus-element-hiding-helper "Incompatible con versiones más recientes de firefox-esr">
<correction all-in-one-sidebar "Incompatible con versiones más recientes de firefox-esr">
<correction autofill-forms "Incompatible con versiones más recientes de firefox-esr">
<correction automatic-save-folder "Incompatible con versiones más recientes de firefox-esr">
<correction classic-theme-restorer "Incompatible con versiones más recientes de firefox-esr">
<correction colorfultabs "Incompatible con versiones más recientes de firefox-esr">
<correction custom-tab-width "Incompatible con versiones más recientes de firefox-esr">
<correction dactyl "Incompatible con versiones más recientes de firefox-esr">
<correction downthemall "Incompatible con versiones más recientes de firefox-esr">
<correction dvips-fontdata-n2bk "Paquete vacío">
<correction firebug "Incompatible con versiones más recientes de firefox-esr">
<correction firegestures "Incompatible con versiones más recientes de firefox-esr">
<correction firexpath "Incompatible con versiones más recientes de firefox-esr">
<correction flashgot "Incompatible con versiones más recientes de firefox-esr">
<correction form-history-control "Incompatible con versiones más recientes de firefox-esr">
<correction foxyproxy "Incompatible con versiones más recientes de firefox-esr">
<correction gitlab "Problemas de seguridad abiertos, correcciones difíciles de adaptar">
<correction greasemonkey "Incompatible con versiones más recientes de firefox-esr">
<correction intel-processor-trace "[s390x] Útil solo en arquitecturas Intel">
<correction itsalltext "Incompatible con versiones más recientes de firefox-esr">
<correction knot-resolver "Problemas de seguridad, correcciones difíciles de adaptar">
<correction lightbeam "Incompatible con versiones más recientes de firefox-esr">
<correction livehttpheaders "Incompatible con versiones más recientes de firefox-esr">
<correction lyz "Incompatible con versiones más recientes de firefox-esr">
<correction npapi-vlc "Incompatible con versiones más recientes de firefox-esr">
<correction nukeimage "Incompatible con versiones más recientes de firefox-esr">
<correction openinbrowser "Incompatible con versiones más recientes de firefox-esr">
<correction perspectives-extension "Incompatible con versiones más recientes de firefox-esr">
<correction pwdhash "Incompatible con versiones más recientes de firefox-esr">
<correction python-facebook "Roto por cambios en el proyecto original">
<correction python-tvrage "Inútil tras el cierre de tvrage.com">
<correction reloadevery "Incompatible con versiones más recientes de firefox-esr">
<correction sage-extension "Incompatible con versiones más recientes de firefox-esr">
<correction scrapbook "Incompatible con versiones más recientes de firefox-esr">
<correction self-destructing-cookies "Incompatible con versiones más recientes de firefox-esr">
<correction spdy-indicator "Incompatible con versiones más recientes de firefox-esr">
<correction status-4-evar "Incompatible con versiones más recientes de firefox-esr">
<correction stylish "Incompatible con versiones más recientes de firefox-esr">
<correction tabmixplus "Incompatible con versiones más recientes de firefox-esr">
<correction tree-style-tab "Incompatible con versiones más recientes de firefox-esr">
<correction ubiquity-extension "Incompatible con versiones más recientes de firefox-esr">
<correction uppity "Incompatible con versiones más recientes de firefox-esr">
<correction useragentswitcher "Incompatible con versiones más recientes de firefox-esr">
<correction video-without-flash "Incompatible con versiones más recientes de firefox-esr">
<correction webdeveloper "Incompatible con versiones más recientes de firefox-esr">
<correction xul-ext-monkeysphere "Incompatible con versiones más recientes de firefox-esr">

</table>

<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas por
esta nueva versión en la distribución «estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «estable» actual:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Actualizaciones propuestas a la distribución «estable»:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Información sobre la distribución «estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>


