#use wml::debian::template title="Overzetting naar PowerPC (APUS)" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/powerpc/inst/menu.inc"
#use wml::debian::translation-check translation="a9738774706d265243f4d1b6f43b411f8536f5c8"

<h1>Debian GNU/Linux installeren op een PowerPC APUS-machine</h1>

<p>

Apus is het "Amiga PowerUp Systeem", en bestaat uit een Amiga computer, A1200, A3000 of A4000, samen met een van de PowerPC/m68k acceleratorborden van het inmiddels ter ziele gegane bedrijf Phase5, of het BlizzardPPC of CyberStormPPC bord.

<h2>Bekende Problemen</h2>
<p>

Er zijn wat problemen met opstarten op systemen die niet alleen PowerUp ofwel WarpUp gebruiken, maar een hybride mutant van beide. Ik ben hier niet erg bekend mee omdat ik alleen PowerUp gebruik, dus ik hoop dat iemand een completere beschrijving kan geven.
<p>

Ook wordt de SCSI-controller van de CyberStormPPC nog niet ondersteund door Linux, dus kunt u er geen schijven op aansluiten.

<h2>Schijfindeling vanuit de kant van het AmigaOS</h2>
<p>

<code>amiga-fdisk</code> is de fdisk-variant voor RDB-partitietabellen die worden gebruikt door de Amiga-hardware. Dit werkt, maar ik raad aan om in plaats daarvan de Amiga schijfindelingshulpmiddelen te gebruiken om het vanuit AmigaOS te doen.
<p>

HDToolbox, het officiële schijfindelingsgereedschap van Commodore, zou op elk AmigaOS systeem geïnstalleerd moeten zijn. Het starten van HDToolbox is voldoende om de IDE-schijf van de onboard IDE-interface in te delen. Als u toegang wilt tot de SCSI-schijf op de SCSI-controller van uw BlizzardPPC-bord, moet u het commando "hdtoolbox blizzppcscsi.device" gebruiken.
<p>

Een andere optie is om SCSIConfig te gebruiken, het schijfindelingshulpmiddel van Phase5 dat op de diskettes staat die bij uw acceleratorbord zijn geleverd.
<p>

U moet het partitietype instellen op custom/aangepast en de volgende partitietype-ID's opgeven:
<pre>
  * Linux partition: 0x4c4e5800
  * Linux swap partition: 0x53575000
</pre>

<h2>Bootstrap</h2>
<p>

U vindt het programma <code>bootstrap</code> in de map <code>apus/bootstrap</code> van de opstartdiskette van de powerpc-distributie (te vinden op /debian/dists/woody/main/disks-powerpc/current).
<p>

Het programma <code>bootstrap</code> bestaat uit drie programma's. Ze moeten alle drie uitvoerbaar zijn en zich in uw AmigaOS-pad bevinden. Dit zijn het uitvoerbare bestand <code>bootstrap</code> en het ppcboot_wup- of ppcboot_pup-gedeelte, dat wil zeggen het eigenlijke opstartprogramma (ppcboot_pup voor het powerup-systeem en ppcboot_wup voor het warpup-systeem).
<p>

U start <code>bootstrap</code> met een regel als deze:
<pre>
\# bootstrap --apus "kernelopties"
</pre>
waarbij "kernelopties" worden gedefinieerd in de volgende secties.
<p>

<code>bootstrap</code> zal dan wat uitvoer geven, dan het scherm 10 tot 30 seconden leeg maken en dan heeft u een Linux-console.

<h3>Het Debian <code>bootstrap</code>-commando</h3>
<p>

Het eigenlijke <code>bootstrap</code>-commando om het Debian installatiesysteem te starten zou dan zijn:
<pre>
\# bootstrap --apus -k apus/linux -r apus/images-1.44/root.bin root=/dev/ram
</pre>
Na de installatie kunt u Debian starten met:
<pre>
\# bootstrap --apus -k apus/linux root=/dev/sda3
</pre>
waarbij sda3 mijn Debian-rootpartitie is, verander dit in de partitie waarop uw rootpartitie staat.


<h2>Kernelopties</h2>
<p>
Afhankelijk van uw daadwerkelijke configuratie moet u enkele kernelopties toevoegen, die in de volgende secties worden uitgelegd.

<h3>Opties voor grafische kaarten</h3>
<p>

Het te gebruiken grafische apparaat is een optie voorafgegaan door video=. Enkele voorbeelden vindt u hieronder. Om de oorspronkelijke grafische weergave in de VGA-modus (640x480) in te schakelen, gebruikt u:
<pre>
  video=amifb:vga
</pre>
Om de grafische kaart Bvision/CyberVision in te schakelen in de modus 1152x864 bij 60Hz met de SUN12x22 lettertypen, gebruikt u:
<pre>
  video=pm2fb:mode:1152x864-60,font:SUN12x22
</pre>

Om een van de grafische apparaten uit te schakelen, gebruikt u:
<pre>
  video=amifb:disable
</pre>
U kunt virtuele consoles toewijzen aan de verschillende apparaten die worden gebruikt. Gebruik
<tt>
  video=map:01
</tt>
om virtuele console (vc) 1 toe te wijzen aan apparaat 0, vc 2 aan apparaat 1, en herhaal daarna hetzelfde patroon (vc3 aan apparaat 0, vc4 aan apparaat 1, enzovoort). Om vc 1,2,3,5,6,7 toe te wijzen aan apparaat 0 en vc 4,8 aan apparaat 1, gebruikt u
<pre>
  video=map:0001
</pre>


<h3>De optie nobats</h3>
<p>

Blizzard-gebruikers met scsi-schijven moeten de optie "nobats" gebruiken.
<pre>
\# bootstrap --apus -k apus/linux root=/dev/sda3 nobats
</pre>

<h3>De optie 60nsram</h3>
<p>
Mensen met 60ns-ram kunnen ook de optie 60nsram gebruiken.
<pre>
\# bootstrap --apus -k apus/linux root=/dev/sda3 60nsram
</pre>

<h3>Debugopties</h3>
<p>
Als er problemen zijn, kan de optie debug gebruikt worden om aan te geven dat de uitvoer van consoleberichten naar een seriële console of geheugen moet gaan in plaats van naar de normale console. Dit is handig om te debuggen als de kerneluitvoer niet naar de consoleuitvoer komt.
<pre>
\# bootstrap --apus -k apus/linux root=/dev/sda3 60nsram debug=mem
</pre>
Vervolgens kunt u het resultaat lezen met het hulpprogramma bootmesg uit de map apus/bootstrap.
<p>

Een ander nuttig hulpmiddel is het hulpprogramma dmesg, dat u informatie geeft voor het opsporen van fouten in het bootstrap-proces.

<h2>Bijzonderheden in verband met Apus in <code>dbootstrap</code></h2>
<p>

In het gebruik van <code>dbootstrap</code> zijn er voor apus enkele specifieke verschillen.

<h3>Indelen van de harde schijf - <code>amiga-fdisk</code></h3>
<p>

Voor de subarchitectuur apus wordt het schijfindelingshulpmiddel <code>amiga-fdisk</code> gebruikt. Zoals hierboven vermeld, kunt u ook schijfindelingsgereedschap gebruiken van de kant van AmigaOS.

<h3>De OS-kernel en modules installeren</h3>
<p>

Deze optie werkt eigenlijk niet. Ik ben bezig met het voorstellen van een optie "De OS-Modules Installeren" om deze te vervangen, maar in de tussentijd kunt u deze stap gewoon overslaan. Hoe dan ook is de kernel is niet nodig, omdat deze zich bevindt op de ...
# <!-- FIXME Sven?? server? network? -->

<h3>Opties die niet van toepassing zijn voor apus</h3>
<p>

Sommige opties hebben gewoon geen zin op apus, dus negeer ze gewoon totdat ik ze weghaal uit het menu. Hoe dan ook zllen ze toch niet werken.

<p>
Deze opties zijn:
<pre>
* Het systeem direct opstartbaar maken vanaf de harde schijf.

* Een opstartdiskette maken.

* De diskette uitwerpen.
</pre>

<h2>Links naar bijkomende informatie</h2>
<p>

De officiële Linux-apus documentatie en FAQ bevindt zich op:
<p>
<a href="http://sourceforge.net/projects/linux-apus/">
http://sourceforge.net/projects/linux-apus/</a>
<p>

Een andere bron van waardevolle informatie is de Linux-m68k website en faq die u kunt vinden op:

<p>
<a href="http://sourceforge.net/projects/linux-m68k/">
http://sourceforge.net/projects/linux-m68k/</a>
<p>

Daar vindt u veel informatie over Linux op het amiga-platform welke gemeenschappelijk is voor Linux-m68k en Linux-apus.

<h2>Conclusie</h2>
<p>

Deze kleine gids probeert alle bijzonderheden van de Linux-apus installatie van Debian uit te leggen. De rest is vergelijkbaar met elke andere Debian/powerpc-installatie en met de algemene installatie van Debian. U vindt dus meer informatie in de <a href="$(DOC)/">map met Debian-documentatie</a>, evenals in de andere algemene Linux-informatiesites en -documentatie.
