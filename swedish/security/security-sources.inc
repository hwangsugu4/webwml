
<ul>
<li><a href="https://security-tracker.debian.org/">Debians säkerhetsspårare</a>
primär källa för all säkerhetsrelaterad information, sökalternativ</li>

<li><a href="https://security-tracker.debian.org/tracker/data/json">JSON-lista</a>
  innehåller CVE-beskrivning, paketnamn, Debian-felnummer, paketversioner med rättning, ingen DSA inkluderad
</li>

<li><a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DSA/list">DSA-lista</a>
  innehåller DSA inklusive datum, relaterade CVE-nummer, paketversioner med rättning
</li>

<li><a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DLA/list">DLA-lista</a>
  innehåller DLA inklusive datum, relaterade CVE-nummer, paketversioner med rättning
</li>

<li><a href="https://lists.debian.org/debian-security-announce/">
DSA-tillkännagivanden</a> (Debian säkerhetsbulletiner)</li>
<li><a href="https://lists.debian.org/debian-lts-announce/">
DLA-tillkännagivanden</a> (Debian säkerhetsbulletiner från Debian LTS)</li>

<li> <a class="rss_logo" style="float: none;" href="$(HOME)/security/dsa">RSS</a>
av DSA eller <a class="rss_logo" style="float: none;" href="$(HOME)/security/dsa-long">RSS</a> lång version inklusive bulletinens text</li>

<li> <a class="rss_logo" style="float: none;" href="$(HOME)/lts/security/dla">RSS</a>
av DLA eller <a class="rss_logo" style="float: none;" href="$(HOME)/lts/security/dla-long">RSS</a> lång version inklusive bulletinens text</li>

<li><a href="oval">Oval-filer</a></li>

<li>Kolla upp en DSA (stora bokstäver är viktigt)<br>
t.ex. <tt>https://security-tracker.debian.org/tracker/DSA-3814</tt>
</li>

<li>Kolla upp en DLA ( -1 är viktigt)<br>
t.ex. <tt>https://security-tracker.debian.org/tracker/DLA-867-1</tt>
</li>

<li>Kolla upp en CVE<br>
t.ex. <tt>https://security-tracker.debian.org/tracker/CVE-2017-6827</tt>
</li>
</ul>
