msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2023-11-23 13:53+0000\n"
"Last-Translator: Debian l10n Arabic <debian-l10n-arabic@lists.debian.org>\n"
"Language-Team: Debian l10n Arabic <debian-l10n-arabic@lists.debian.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Poedit 3.2.2\n"

#. Translators: string printed by gpg --fingerprint in your language, including spaces
#: ../../english/CD/CD-keys.data:4 ../../english/CD/CD-keys.data:8
#: ../../english/CD/CD-keys.data:12
msgid "      Key fingerprint"
msgstr "      مفتاح بصمة الأصبع"

#: ../../english/devel/debian-installer/images.data:89
msgid "ISO images"
msgstr "صور ISO"

#: ../../english/devel/debian-installer/images.data:90
msgid "Jigdo files"
msgstr "ملفات جيكدو"

#. note: only change the sep(arator) if it's not good for your charset
#: ../../english/template/debian/cdimage.wml:10
msgid "&middot;"
msgstr "&middot;"

#: ../../english/template/debian/cdimage.wml:13
msgid "<void id=\"dc_faq\" />FAQ"
msgstr "<void id=\"dc_faq\" />الأسئلة الشائعة"

#: ../../english/template/debian/cdimage.wml:16
msgid "Download with Jigdo"
msgstr "نزّل بجيكدو"

#: ../../english/template/debian/cdimage.wml:19
msgid "Download via HTTP/FTP"
msgstr "نزّل عبر الـ HTTP/FTP"

#: ../../english/template/debian/cdimage.wml:22
msgid "Buy CDs or DVDs"
msgstr "شراء الأقراص المدمجة CDs وأقراص الفيديو الرقمية DVDs"

#: ../../english/template/debian/cdimage.wml:25
msgid "Network Install"
msgstr "التثبيت الشبَكي"

#: ../../english/template/debian/cdimage.wml:28
msgid "<void id=\"dc_download\" />Download"
msgstr "<void id=\"dc_download\" />نزّل"

#: ../../english/template/debian/cdimage.wml:31
msgid "<void id=\"dc_misc\" />Misc"
msgstr "<void id=\"dc_misc\" />متفرقات"

#: ../../english/template/debian/cdimage.wml:34
msgid "<void id=\"dc_artwork\" />Artwork"
msgstr "التثبيت الشبَكي"

#: ../../english/template/debian/cdimage.wml:37
msgid "<void id=\"dc_mirroring\" />Mirroring"
msgstr "<void id=\"dc_mirroring\" />المرايا"

#: ../../english/template/debian/cdimage.wml:40
msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
msgstr "<void id=\"dc_rsyncmirrors\" />المرايا المتزامنة"

#: ../../english/template/debian/cdimage.wml:43
msgid "<void id=\"dc_verify\" />Verify"
msgstr "<void id=\"dc_verify\" />تحقق"

#: ../../english/template/debian/cdimage.wml:46
msgid "<void id=\"dc_torrent\" />Download with Torrent"
msgstr "<void id=\"dc_torrent\" />نزّل بالتورنت"

#: ../../english/template/debian/cdimage.wml:49
msgid "Debian CD team"
msgstr "فريق دبيان CD"

#: ../../english/template/debian/cdimage.wml:52
msgid "debian_on_cd"
msgstr "دبيان_على_الأقراص_المدمجة"

#: ../../english/template/debian/cdimage.wml:55
msgid "<void id=\"faq-bottom\" />faq"
msgstr "<void id=\"faq-bottom\" />الأسئلة الشائعة"

#: ../../english/template/debian/cdimage.wml:58
msgid "jigdo"
msgstr "جيكدو"

#: ../../english/template/debian/cdimage.wml:61
msgid "http_ftp"
msgstr "http_ftp"

#: ../../english/template/debian/cdimage.wml:64
msgid "buy"
msgstr "شراء"

#: ../../english/template/debian/cdimage.wml:67
msgid "net_install"
msgstr "التثبيت_الشبَكي"

#: ../../english/template/debian/cdimage.wml:70
msgid "<void id=\"misc-bottom\" />misc"
msgstr "<void id=\"misc-bottom\" />متفرقات"

#: ../../english/template/debian/cdimage.wml:73
msgid ""
"English-language <a href=\"/MailingLists/disclaimer\">public mailing list</"
"a> for CDs/DVDs:"
msgstr ""
"English-language <a href=\"/MailingLists/disclaimer\">public mailing list</"
"a> for CDs/DVDs:"
