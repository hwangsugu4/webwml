#use wml::debian::template title="Inleiding tot Debian" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="825e6f3e9f80b31f27c2b6a4590b0e29a084fe21"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="community">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>

      <a id=community></a>
      <h2>Debian is een gemeenschap</h2>
      <p>Duizenden vrijwilligers van over de hele wereld werken samen aan het Debian-besturingssysteem met Vrije en Openbronsoftware als prioriteit. Maak kennis met het Debian-project.</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="people">De mensen:</a>
          Wie we zijn en wat we doen
        </li>
        <li>
          <a href="philosophy">Onze filosofie:</a>
          Waarom we het doen en hoe we het doen
        </li>
        <li>
          <a href="../devel/join/">Meewerken:</a>
          Hoe u een medewerker van Debian kunt worden
         </li>
         <li>
           <a href="help">Bijdragen:</a>
           Hoe u Debian kunt helpen
         </li>
         <li>
           <a href="../social_contract">Het sociaal contract van Debian:</a>
           Onze morele agenda
          </li>
          <li>
            <a href="diversity">Iedereen is welkom:</a>
            De diversiteitsverklaring van Debian
           </li>
           <li>
             <a href="../code_of_conduct">Voor deelnemers:</a>
             De gedragscode van Debian
           </li>
           <li>
             <a href="../partners/">Partners:</a>
             Bedrijven en organisaties die het Debian-project ondersteunen
            </li>
            <li>
              <a href="../donations">Giften:</a>
              Hoe u Debian kunt sponsoren
             </li>
             <li>
               <a href="../legal/">Juridische aangelegenheden:</a>
               Licenties, handelsmerken, privacybeleid, octrooibeleid, enz.
             </li>
             <li>
               <a href="../contact">Contact opnemen:</a>
               Hoe u met ons in contact kunt komen
              </li>
            </ul>
    </div>

  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-desktop fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
     <a id=software></a>
     <h2>Debian is een besturingssysteem</h2>
     <p>Debian is een vrij besturingssysteem dat ontwikkeld en onderhouden wordt door het Debian-project. Het is een vrije Linux-distributie met duizenden toepassingen om tegemoet te komen aan de behoeften van onze gebruikers.</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="../distrib">Downloaden:</a>
          Waar u Debian kunt verkrijgen
        </li>
        <li>
          <a href="why_debian">Waarom Debian:</a>
          Redenen om voor Debian te kiezen
        </li>
        <li>
           <a href="../support">Ondersteuning:</a>
           Waar u hulp kunt vinden
        </li>
        <li>
          <a href="../security">Beveiliging:</a>
          Recente adviezen en bronnen van beveiligingsinformatie
        </li>
        <li>
          <a href="../distrib/packages"> Software:</a>
          Zoeken en bladeren in de lange lijst met pakketten van Debian
         </li>
         <li>
           <a href="../doc"> Documentatie:</a>
           Installatiehandleiding, FAQ, HOWTO's, Wiki en meer
          </li>
          <li>
            <a href="../bugs"> Bugvolgsysteem (Bug Tracking System - BTS):</a>
            Hoe u een bug kunt melden, BTS-documentatie
          </li>
          <li>
            <a href="https://lists.debian.org/">Mailinglijsten:</a>
            Een geheel van Debian-lijsten voor gebruikers, ontwikkelaars, enz.
          </li>
          <li>
             <a href="../blends"> Specifieke uitgaven (pure blends):</a>
             Metapakketten die aan specifieke behoeften beantwoorden
           </li>
           <li>
             <a href="../devel"> Ontwikkelaarshoek:</a>
             Informatie die voornamelijk voor de ontwikkelaars van Debian van belang is
            </li>
            <li>
               <a href="../ports"> Ports/Architecturen:</a>
               Ondersteuning van Debian voor verschillende CPU-architecturen
             </li>
             <li>
               <a href="search">Zoeken:</a>
               Informatie over het gebruik van de Debian-zoekmachine
              </li>
              <li>
                <a href="cn">Talen:</a>
                Taalinstellingen voor de website van Debian
              </li>
            </ul>
    </div>
  </div>

</div>

