#use wml::debian::template title="Debian GNU/Hurd --- Documentação" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/hurd/menu.inc"
#use wml::debian::translation-check translation="55a70d0c0f3df8d4df237334ac6de72daaa99f73"

<h1>Debian GNU/Hurd</h1>
<h2>Tradutores</h2>
<ul>
<li><a href="#concept" name="TOC_concept">Conceito</a></li>
<li><a href="#examples" name="TOC_examples">Exemplos</a></li>
<li><a href="#actpas" name="TOC_actpas">Tradutores passivos, tradutores ativos</a></li>
<li><a href="#manage" name="TOC_manage">Gerenciando tradutores</a></li>
</ul>

<h3><a href="#TOC_concept" name="concept">Conceito</a></h3>
<p>
Antes de examinarmos os tradutores, consideremos os sistemas de arquivos
regulares. Um sistema de arquivo é um armazenamento para uma árvore
hierárquica de diretórios e arquivos. Você acessa os diretórios e os arquivos
através de uma string de caracteres especial, o caminho (path, em inglês).
Além disso, existem links simbólicos para referenciar um arquivo em
diversos lugares na árvore, existem hard links para dar diversos nomes
a um mesmo e único arquivo. Existem também arquivos de dispositivo especiais
para comunicação com os drivers de dispositivos de hardware do kernel, e
existem pontos de montagem para incluir outros armazenamentos na árvore
de diretórios. E existem objetos obscuros como os FIFOs.</p>
<p>
Embora esses objetos sejam muito diferentes, eles compartilham de algumas
propriedades comuns, por exemplo, todos eles têm um(a) proprietário(a) e um
grupo associados, como também direitos de acesso (permissões). Esta informação
fica escrita em inodes. Isto é, na verdade, uma convergência adicional: todo
objeto tem exatamente um inode associado a ele (hard links são algo de
especial já que eles compartilham um mesmo e único inode). Às vezes, o inode
tem informações adicionais armazenadas nele. Por exemplo, o inode pode conter
o alvo de um link simbólico.</p>
<p>
Contudo, esses aspectos comuns geralmente não são aproveitados nas
implementações, apesar da interface comum de programação entre eles. Todos os
inodes podem ser acessados através de chamadas POSIX padrões, por exemplo,
<code>read()</code> e <code>write()</code>. Ou para adicionar um
novo tipo de objeto (por exemplo, um novo tipo de link) para um kernel unix
comum e monolítico, você precisaria modificar o código para cada sistema
de arquivos separadamente.</p>
<p>
No Hurd, as coisas funcionam diferentemente. Embora no Hurd um servidor
especial de sistema de arquivos possa explorar propriedades especiais de
tipos de objeto padrões, como os links (no sistema de arquivo ext2 com links
rápidos, por exemplo), ele tem uma interface genérica para adicionar tais
funcionalidades sem modificar o código existente.</p>
<p>
O truque está em permitir a um programa que se insira entre o atual conteúdo
de um arquivo e o(a) usuário(a) que está acessando este arquivo. Tal programa
é chamado de tradutor, porque ele é capaz de processar requisições de entrada
por muitas e diferentes maneiras. Em outras palavras, um tradutor é um
servidor Hurd que fornece uma interface básica de sistema de arquivos.</p>
<p>
Os tradutores têm propriedades muito interessantes. Do ponto de vista do
kernel, eles são apenas outro processo de usuário(a). Isto é, os tradutores
podem ser executados por quaisquer usuários(as). Você não precisa de
privilégios de root para instalar ou para modificar um tradutor, você somente
precisa dos direitos de acesso para o inode subjacente ao tradutor ao qual
está anexado. Muitos tradutores não requerem um arquivo real para operar,
eles podem fornecer informações por seus próprios meios. Este é o motivo
da informação sobre tradutores ficar armazenada no inode.</p>
<p>
Os tradutores são responsáveis por servir todas as operações de sistema
de arquivo que envolvem o inode no qual estão anexados. Porque não estão
restritos aos usuais conjuntos de objetos (arquivo de dispositivo, link, etc.),
eles estão livres para retornar qualquer coisa que faça sentido ao(à)
programador(a). Pode-se imaginar um tradutor que se comporta como um
diretório quando acessado por <code>cd</code> ou <code>ls</code> e, ao
mesmo tempo, se comporta como um arquivo quando acessado por
<code>cat</code>.</p>

<h3><a href="#TOC_examples" name="examples">Exemplos</a></h3>
<h4>Pontos de montagem</h4>
<p>
Um ponto de montagem pode ser visto como um inode que tem um tradutor
especial anexado a ele. Seu propósito seria traduzir as operações do
sistema de arquivos no ponto de montagem em operações de sistema de arquivos
em outro armazenamento, por exemplo, em uma outra partição.</p>
<p>
De fato, é assim que os sistemas de arquivos são implementados sob o Hurd.
Um sistema de arquivos é um tradutor. Este tradutor pega um armazenamento
como seu argumento e é capaz de servir todas as operações de sistema de
arquivos transparentemente.</p>

<h4>Arquivos de dispositivo</h4>
<p>
Existem muitos e diferentes arquivos de dispositivos, e em sistemas de
kernel monolítico, eles são todos fornecidos pelo kernel mesmo. No Hurd,
todos os arquivos de dispositivo são fornecidos por tradutores. Um
tradutor pode fornecer suporte para vários arquivos de dispositivo similares,
por exemplo, todas as partições do disco rígido. Deste modo, o número de
tradutores realmente existentes é muito pequeno. Contudo, note que
para cada arquivo de dispositivo acessado, uma tarefa de tradutor separada
é iniciada. Porque o Hurd é profundamente multithread, isso sai muito
barato.</p>
<p>
Quando hardware é envolvido, um tradutor geralmente começa a se comunicar com
o kernel para obter os dados do hardware. Entretanto, se não é preciso acesso
ao hardware, o kernel não precisa estar envolvido. Por exemplo,
<code>/dev/zero</code> não requer acesso ao hardware e, portanto, pode ser
implementado completamente no espaço de usuário(a).</p>

<h4>Links simbólicos</h4>
<p>
Um link simbólico pode ser visto como um tradutor. Ao acessar o link
simbólico, um tradutor seria iniciado, o qual encaminharia a requisição
para o sistema de arquivos que contenha o arquivo para o qual o link
aponta.</p>
<p>
Contudo, para uma melhor performance, os sistemas de arquivos que possuem
suporte nativo aos links simbólicos podem tirar vantagem desta funcionalidade
e implementar links simbólicos de maneira diferente. Internamente, o acesso
a um link simbólico não iniciaria um novo processo de tradutor. Porém, para
o(a) usuário(a), ainda pareceria como se o tradutor passivo estivesse
envolvido (veja abaixo uma explicação sobre o que é um tradutor passivo).</p>
<p>
Devido ao Hurd vir com um tradutor de symlink, qualquer servidor de sistema
de arquivos que fornece suporte para tradutores automaticamente tem suporte
para symlinks (e firmlinks, e arquivos de dispositivos, etc.)! Ou seja,
você pode conseguir um sistema de arquivos funcionando bem rápido e adicionar
suporte nativo aos symlinks e a outras funcionalidades mais tarde.</p>

<h3><a href="#TOC_actpas" name="actpas">Tradutores passivos, tradutores ativos</a></h3>
<p>
Existem dois tipos de tradutores, passivos e ativos. Eles realmente são
coisas completamente diferentes, então não confunda; mas eles mantêm uma
relação próxima.</p>

<h4>Tradutores ativos</h4>
<p>
Um tradutor ativo é um processo de tradutor em execução, como visto acima.
Você pode definir e remover tradutores ativos usando o comando
<code>settrans -a</code>.
A opção <code>-a</code> é necessária para dizer ao
<code>settrans</code> que você quer modificar o tradutor ativo.</p>
<p>
O comando <code>settrans</code> recebe três tipos de argumentos. Primeiro, você
pode definir opções para o comando <code>settrans</code> mesmo, como
<code>-a</code> para modificar o tradutor ativo. A seguir você define o inode
que quer modificar. Lembre-se que um tradutor está sempre associado com um
inode na hierarquia de diretórios. Você somente pode modificar um inode
por vez. Se você não especificar mais nenhum argumento, <code>settrans</code>
tentará remover um tradutor existente. O quanto ele vai tentar depende das
opções de imposição que você especificar (se o tradutor está em uso por
qualquer processo, você obterá uma mensagem de erro "device or resource busy"
- dispositivo ou recurso ocupado - a menos que você force a remoção).</p>
<p>
Mas se você especificar argumentos adicionais, eles serão interpretados como
uma linha de comando para executar o tradutor. Isto é, o próximo argumento
é o nome de arquivo do executável do tradutor. Argumentos adicionais são
opções para o tradutor, e não para o comando <code>settrans</code>.</p>
<p>
Por exemplo, para montar uma partição ext2fs, você pode executar
<code>settrans -a -c /mnt /hurd/ext2fs /dev/hd2s5</code>. A opção
<code>-c</code> criará um ponto de montagem se ele ainda não
existir. Aliás, não precisa ser um diretório. Para desmontar, você tentaria
<code>settrans -a /mnt</code>.</p>

<h4>Tradutores passivos</h4>
<p>
Um tradutor passivo é definido e modificado pela mesma sintaxe dos tradutores
ativos (somente ignore a opção <code>-a</code>), então tudo o que foi dito
acima também é verdade para os tradutores passivos. Contudo, existe uma
diferença: tradutores passivos ainda não foram iniciados.</p>
<p>
Isto faz sentido porque isto é o que você geralmente quer. Você não quer a
partição montada a menos que você realmente acesse os arquivos nesta partição.
Você não quer ligar a rede a menos que exista algum tráfego, e assim por
diante.</p>
<p>
Ao contrário, a primeira vez que um tradutor passivo é acessado, ele é
automaticamente lido do inode e um tradutor ativo é iniciado a partir
dele usando a linha de comando que foi armazenada no inode. Isto é
similar à funcionalidade de montagem automática do Linux. Contudo, ele não
vem como um bônus que você tem que configurar manualmente, mas como uma
parte integral do sistema. Assim, definir tradutores passivos posterga o
início da tarefa de tradutor até que você realmente precise dela. A propósito,
se o tradutor ativo morre por alguma razão, a próxima vez em que o inode
for acessado, o tradutor é reiniciado.</p>
<p>
Existe uma outra diferença: tradutores ativos podem morrer ou se perder.
Logo após o término do processo do tradutor ativo (por exemplo, porque você
reiniciou a máquina), ele estará perdido para sempre. Tradutores passivos não
são transientes e ficam no inode durante a reinicialização até que você
modifique-os com o programa <code>settrans</code> ou apague os inodes aos
quais estão anexados. Isto significa que você não precisa manter um
arquivo de configuração com seus pontos de montagem.</p>
<p>
Um último ponto: mesmo se você definiu um tradutor passivo, você ainda pode
definir um tradutor ativo diferente. Somente se o tradutor é automaticamente
iniciado porque não existia tradutor ativo quando o inode foi acessado, assim
o tradutor passivo é considerado.</p>

<h3><a href="#TOC_manage" name="manage">Gerenciando tradutores</a></h3>
<p>
Como mencionado acima, você pode usar
<code>settrans</code>
para definir e alterar tradutores passivos e ativos. Existem muitas opções
para alterar o comportamento do <code>settrans</code> no caso de algo dar
errado e para condicionar suas ações. Aqui estão alguns usos comuns:</p>
<ul><li><code>settrans -c /mnt /hurd/ext2fs /dev/hd2s5</code> monta uma
partição, o tradutor permanece após a reinicialização.</li>
<li><code>settrans -a /mnt /hurd/ext2fs ~/dummy.fs</code> monta um
sistema de arquivos dentro de um arquivo de dados, o tradutor some se ele
morrer.</li>
<li><code>settrans -fg /nfs-data</code> força um tradutor a sumir.</li>
</ul>
<p>
Você pode usar o comando
<a href="hurd-doc-utils#showtrans"><code>showtrans</code></a>
para ver se um tradutor está anexado a um inode. Porém, isso só vai mostrar
o tradutor passivo.</p>
<p>
Você pode alterar as opções de um tradutor ativo (de sistema de arquivos) com
<code>fsysopts</code> sem realmente reiniciá-lo. Isto é muito
conveniente. Por exemplo, você pode fazer o chamado "remontando uma
partição somente-leitura" sob o Linux executando simplesmente <code>fsysopts
/mntpoint --readonly</code>. O tradutor ativo em execução
mudará seu comportamento de acordo com sua requisição, se possível. O
<code>fsysopts /mntpoint</code> sem nenhum parâmetro mostra as configurações
atuais.</p>

<h4>Exemplos</h4>
<p>
Eu recomendo que você inicie pela leitura do comando <code>/bin/mount</code>,
é só um pequeno script. Devido à configuração de tradutores de sistemas de
arquivos ser similar à montagem de partições, você facilmente pode captar
o conceito desta maneira. Crie uma imagem de sistema de arquivo com
<code>dd if=/dev/zero of=dummy.fs bs=1024k
count=8; mke2fs dummy.fs</code> e "monte-a" com <code>settrans -c dummy
/hurd/ext2fs `pwd`/dummy.fs</code>. Note que o tradutor ainda não
iniciou, nenhum novo processo <code>ext2fs</code> está executando (verifique
com <code>ps Aux</code>). Verifique se tudo está correto usando
<code>showtrans</code>.</p>
<p>
Agora digite <code>ls dummy</code> e você perceberá um curto atraso que
acontece enquanto o tradutor é iniciado. Após isso, não haverá mais atrasos
acessando o dummy. Sob o Linux, poderia se dizer que você montou automaticamente
um sistema de arquivo em loop. Verifique com <code>ps Aux</code> que agora
existe um processo <code>ext2fs dummy</code> ativo e executando. Agora
coloque alguns arquivos no novo diretório. Tente fazer o sistema de arquivos
ficar em somente-leitura com <code>fsysopts</code>.
Note quão longe vão as falhas das tentativas de escrita. Tente matar o
tradutor ativo com <code>settrans -g</code>.</p>
<p>
Agora você deve ter alguma compreensão do que está acontecendo. Lembre-se
de que foi somente <em>um</em> servidor especial, o servidor ext2fs do Hurd.
Existem muitos mais servidores no diretório <code>hurd</code>. Alguns deles
são para sistemas de arquivos. Alguns são necessários para funcionalidades
de sistemas de arquivo, como os links. Alguns são necessários para arquivos
de dispositivos. Alguns são úteis para redes. Imagine "montar" um servidor
FTP com <code>settrans</code> e baixar arquivos
simplesmente com o comando padrão <code>cp</code>. Ou editar seus sites web
com <code>emacs /ftp/homepage.my.server.org/index.html</code>!</p>
