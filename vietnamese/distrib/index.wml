#use wml::debian::translation-check translation="0691e0b35ed0aa5df10a4b47799051f77e519d25" maintainer="Trần Ngọc Quân"
#use wml::debian::template title="Lấy Debian"
#include "$(ENGLISHDIR)/releases/images.data"

<p>Debian được phân phối <a href="../intro/free">tự do</a>
thông qua Internet. Bạn có thể tải mọi thứ về nó từ bất kỳ
<a href="ftplist">máy bản sao</a> nào của chúng tôi.
Phần <a href="../releases/stable/installmanual">hướng dẫn cài đặt</a>
chứa chi tiết các bước cần thiết để cài đặt.
Và các ghi chú về lần phát hành có thể tìm thấy ở <a href="../releases/stable/releasenotes">đây</a>.
</p>

<p>Trang này có các tùy chọn dành cho cài đặt bản phát hành Debian ổn định. Nếu bạn thích bản
   Thử nghiệm hoặc Không ổn định thì hãy xem <a href="../releases/">trang phát hành</a> của chúng tôi.</p>

<div class="line">
  <div class="item col50">
    <h2><a href="netinst">Tải về một ảnh cài đặt</a></h2>
    <p>Tùy thuộc vào kết nối Internet của bạn, bạn có thể tải về một trong những ảnh đĩa sau:</p>
    <ul>
      <li><a href="netinst"><strong>Ảnh cài đặt nhỏ</strong></a>:
	    có thể tải về nhanh chóng và có thể ghi lên đĩa di động.
	    Để dùng nó, máy tính của bạn cần phải kết nối vào mạng
	    Internet.
	<ul class="quicklist downlist">
	  <li><a title="Tải bộ cài đặt cho máy tính để bàn Intel và AMD 64-bít"
	         href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">64-bit
	      PC netinst iso</a></li>
	  <li><a title="Tải bộ cài đặt cho máy tính để bàn Intel và AMD 32-bít thông thư"
		 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">32-bit
	      PC netinst iso</a></li>
	</ul>
      </li>
      <li><a href="../CD/"><strong>Ảnh cài đặt đầy đủ</strong></a> lớn hơn:
      chứa nhiều gói hơn, làm cho nó trở nên dễ dàng khi cài đặt những máy
      mà nó không có kết nối Internet.
	<ul class="quicklist downlist">
	  <li><a title="Tải torrents DVD cho máy tính để bàn Intel và AMD 64-bít"
	         href="<stable-images-url/>/amd64/bt-dvd/">64-bit PC torrents (DVD)</a></li>
	  <li><a title="Tải torrents DVD cho máy tính để bàn Intel và AMD thông thường 32-bít"
		 href="<stable-images-url/>/i386/bt-dvd/">32-bit PC torrents (DVD)</a></li>
	  <li><a title="Tải torrents CD cho máy tính để bàn Intel và AMD 64-bít"
	         href="<stable-images-url/>/amd64/bt-cd/">64-bit PC torrents (CD)</a></li>
	  <li><a title="Tải torrents CD cho máy tính để bàn Intel và AMD thông thường 32-bít"
		 href="<stable-images-url/>/i386/bt-cd/">32-bit PC torrents (CD)</a></li>
	</ul>
      </li>
    </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="https://cloud.debian.org/images/cloud/">Sử dụng ảnh cho Điện toán đám mây Debian</a></h2>
    <p>Một <a href="https://cloud.debian.org/images/cloud/"><strong>ảnh cho điện toán đám mây</strong></a> chính thức, được biên dịch bởi nhóm điện toán đám mây, có thể sử dụng được trên:</p>
    <ul>
      <li>nhà cung cấp OpenStack của bạn, ở định dạng qcow2 hay raw.
      <ul class="quicklist downlist">
	   <li>64-bit AMD/Intel (<a title="Ảnh OpenStack cho 64-bit AMD/Intel qcow2" href="<cloud-current-url/>-generic-amd64.qcow2">qcow2</a>, <a title="Ảnh OpenStack cho 64-bit AMD/Intel raw" href="<cloud-current-url/>-generic-amd64.raw">raw</a>)</li>
       <li>64-bit ARM (<a title="Ảnh OpenStack cho 64-bit ARM qcow2" href="<cloud-current-url/>-generic-arm64.qcow2">qcow2</a>, <a title="Ảnh OpenStack cho 64-bit ARM raw" href="<cloud-current-url/>-generic-arm64.raw">raw</a>)</li>
	   <li>64-bit Little Endian PowerPC (<a title="Ảnh OpenStack cho 64-bit Little Endian PowerPC qcow2" href="<cloud-current-url/>-generic-ppc64el.qcow2">qcow2</a>, <a title="Ảnh OpenStack cho 64-bit Little Endian PowerPC raw" href="<cloud-current-url/>-generic-ppc64el.raw">raw</a>)</li>
      </ul>
      </li>
      <li>Amazon EC2, hoặc là như một ảnh máy hoặc thông qua AWS Marketplace.
	   <ul class="quicklist downlist">
	    <li><a title="Ảnh máy Amazon" href="https://wiki.debian.org/Cloud/AmazonEC2Image/">Ảnh máy Amazon</a></li>
	    <li><a title="AWS Marketplace" href="https://aws.amazon.com/marketplace/seller-profile?id=4d4d4e5f-c474-49f2-8b18-94de9d43e2c0">AWS Marketplace</a></li>
	   </ul>
      </li>
      <li>Microsoft Azure, trên Azure Marketplace.
	   <ul class="quicklist downlist">
            <li><a title="Debian 12 trên Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-12?tab=PlansAndPrice">Debian 12 ("Bookworm")</a></li>
	    <li><a title="Debian 11 trên Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-11?tab=PlansAndPrice">Debian 11 ("Bullseye")</a></li>
	   </ul>
      </li>
    </ul>
 </div>
</div>
<div class="line">
  <div class="item col50">
    <h2><a href="../CD/vendors/">Mua bộ đĩa CD hoặc DVD từ một trong số
      nhà bán đĩa CD Debian</a></h2>

   <p>
      Nhiều nhà cung cấp bán bản phân phối rẻ hơn 5 đô la Mỹ cộng
      phí vận chuyển (kiểm tra trên trang thông tin điện tử của họ xem có vận chuyển quốc tế hay không).
      <br />
      Một số <a href="../doc/books">sách về Debian</a> cũng đi kèm với
      các đĩa CD.
   </p>

   <p>Sau đây là các thuận lợi cơ bản của các đĩa CD:</p>

   <ul>
     <li>Cài đặt từ bộ đĩa CD đơn giản hơn.</li>
     <li>Bạn có thể cài đặt trên các máy tính không có kết nối Internet.</li>
	 <li>Bạn có thể cài Debian (bao nhiêu máy tùy ý) mà không cần
	 tự mình tải về tất cả các gói.</li>
     <li>Đĩa CD có thể được dùng để dễ dàng cứu hộ hệ thống Debian gặp sự cố.</li>
   </ul>

   <h2><a href="pre-installed">Mua máy tính đã
     cài sẵn Debian</a></h2>
   <p>Đây là một số lợi ích của việc này:</p>
   <ul>
    <li>Bạn không cần phải cài đặt Debian.</li>
    <li>Phần cài đặt đã cấu hình sẵn cho phù hợp với phần cứng.</li>
    <li>Nhà cung cấp có thể hỗ trợ kỹ thuật cho bạn.</li>
   </ul>
  </div>

  <div class="item col50 lastcol">
    <h2><a href="../CD/live/">Thử Debian live trước khi cài</a></h2>
    <p>
      Bạn có thể thử Debian bằng cách mồi hệ thống live từ CD, DVD hay USB
      mà không cài bất kỳ tập tin nào lên máy tính. Khi bạn đã sẵn sàng, bạn có thể
      chạy phần cài đặt (bắt đầu từ Debian 10 Buster, đây là <a href="https://calamares.io">Trình
      cài đặt Calamares</a> thân thiện và dễ sử dụng).
      Cung cấp ảnh đáp ứng nhu cầu kích thước,
      ngôn ngữ và các chọn lựa gói của bạn, cách thức này có thể phù hợp với bạn.
      Đọc thêm <a href="../CD/live#choose_live">thông tin về phương thức này</a>
      để giúp bạn ra quyết định.
    </p>
    <ul class="quicklist downlist">
      <li><a title="Tải torrents live cho máy tính để bàn Intel và AMD 64-bít"
	     href="<live-images-url/>/amd64/bt-hybrid/">64-bit PC live torrents</a></li>
    </ul>
  </div>

</div>


# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>
Nếu bất kỳ phần cứng nào trong hệ thống của bạn <strong>yêu cầu non-free firmware
được tải</strong> với trình điều khiển thiết bị, bạn có thể sử dụng một trong số
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
tarballs của các gói firmware hay được dùng</a> hoặc tải về một ảnh <strong>không chính thức</strong>
có chứa những firmwares <strong>non-free</strong> đó. Chỉ dẫn về cách làm thế nào để sử dụng các tarballs
và các thông tin chung về tải firmware trong quá trình cài đặt có thể
tìm thấy trong <a href="../releases/stable/amd64/ch06s04">Hướng dẫn cài đặt</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">ảnh
cài đặt không chính thức cho <q>bản ổn định</q> có chứa cả firmware</a>
</p>
</div>
