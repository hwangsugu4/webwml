#use wml::debian::template title="Informations sur la version « Etch » de Debian"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="28d2aaba8d42e9ca9808154a8580598385f19912" maintainer="Jean-Paul Guillonneau"


<p>La version de Debian GNU/Linux <current_release_etch> a été publiée le
<a href="$(HOME)/News/<current_release_newsurl_etch/>"><current_release_date_etch></a>.
Debian 4.0 a été initialement publiée le <:=spokendate('2007-04-08'):>.

Cette version comprend de nombreuses modifications décrites dans notre
<a href="$(HOME)/News/2007/20070408">communiqué de presse</a> et nos
<a href="releasenotes">notes de publication</a>.</p>


<p><strong>Debian GNU/Linux 4.0 a été remplacée par
<a href="../lenny/">Debian GNU/Linux 5.0 (<q>Lenny</q>)</a>.

La prise en charge des mises à jour de sécurité a cessé depuis le 6 février 2010.
</strong></p>


<p>Pour obtenir et installer Debian, veuillez vous reporter à la page des
informations d'installation et au guide d'installation. Pour mettre à niveau à
partir d'une ancienne version de Debian, veuillez vous reporter aux
instructions des <a href="releasenotes">notes de publication</a>.</p>

<p> les architectures suivantes étaient prises en charge dans cette publication :</p>

<ul>
<li><a href="../../ports/alpha/">Alpha</a>
<li><a href="../../ports/amd64/">PC 64 bits (amd64)</a>
<li><a href="../../ports/arm/">ARM</a>
<li><a href="../../ports/hppa/">HP PA-RISC</a>
<li><a href="../../ports/i386/">PC 32 bits (i386)</a>
<li><a href="../../ports/ia64/">Intel Itanium IA-64</a>
<li><a href="../../ports/mips/">MIPS (gros-boutiste)</a>
<li><a href="../../ports/mipsel/">MIPS (petit-boutiste)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/s390/">IBM S/390</a>
<li><a href="../../ports/sparc/">SPARC</a>
</ul>

<p>
Contrairement à nos souhaits, certains problèmes pourraient toujours exister
dans cette version, même si elle est déclarée <em>stable</em>. Nous avons
réalisé <a href="errata">une liste des principaux problèmes connus</a>, et vous
pouvez toujours nous signaler d'autres problèmes.
</p>
