#use wml::debian::template title="Debian-Installer" NOHEADER="true"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="5d9ed11b1ac37b0f0417096ff186a5fd178b7fa6"
# $Id$
# Translation update: Holger Wansing <linux@wansing-online.de>, 2013-2015, 2017.
# Translation update: Holger Wansing <hwansing@mailbox.org>, 2019, 2020, 2021, 2023.

<h1>Neuigkeiten</h1>

<p><:= get_recent_list('News/$(CUR_YEAR)', '2',
'$(ENGLISHDIR)/devel/debian-installer', '', '\d+\w*' ) :>
<a href="News">Ältere Neuigkeiten</a>
</p>

<h1>Installation mit dem Debian-Installer</h1>

<p>
<if-stable-release release="bookworm">
<strong>Bezüglich offizieller Installationsmedien und Informationen für
Debian <current_release_bookworm></strong> schauen Sie bitte auf die
<a href="$(HOME)/releases/bookworm/debian-installer">Bookworm-Seite</a>.
</if-stable-release>
<if-stable-release release="trixie">
<strong>Bezüglich offizieller Installationsmedien und Informationen für
Debian <current_release_trixie></strong>, schauen Sie bitte auf die
<a href="$(HOME)/releases/trixie/debian-installer">Trixie-Seite</a>.
</if-stable-release>

</p>

<div class="tip">
<p>
  Alle unten verlinkten Images sind für die Version des Debian-Installers, die
  für die nächste Debian-Veröffentlichung entwickelt wird, und installieren
  standardmäßig Debian Testing (<q><current_testing_name></q>).
</p>
</div>

<!-- Shown in the beginning of the release cycle: no Alpha/Beta/RC released yet. -->
<if-testing-installer released="no">

<p>
<strong>Um Debian Testing zu installieren</strong>, empfehlen wir Ihnen die 
Verwendung der <strong>täglich gebauten Images</strong> des Installers. Die
folgenden Images sind verfügbar:
</p>

</if-testing-installer>

<!-- Shown later in the release cycle: Alpha/Beta/RC available, point at the latest one. -->
<if-testing-installer released="yes">

<p>
<strong>Um Debian Testing zu installieren</strong>,
empfehlen wir Ihnen die Verwendung der
<q><strong><humanversion /></strong></q>-Veröffentlichung
des Installers, nachdem Sie dessen <a href="errata">\
Fehlerseite</a> überprüft haben. Die folgenden Images sind für
<humanversion /> verfügbar:
</p>

<h2>Offizielle Veröffentlichung</h2>

<div class="line">
<div class="item col50">
<strong>Netzinstallations-(netinst-)CD-Images</strong>
<netinst-images />
</div>

<div class="item col50 lastcol">
<strong>Netzinstallations-(netinst-)CD-Images (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<netinst-images-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<full-dvd-images />
</div>


</div>


<div class="line">
<div class="item col50">
<strong>CD (via <a href="$(HOME)/CD/jigdo-cd">Jigdo</a>)</strong>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD (via <a href="$(HOME)/CD/jigdo-cd">Jigdo</a>)</strong>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray (via <a href="$(HOME)/CD/jigdo-cd">Jigdo</a>)</strong>
<full-bd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>andere Images (Netboot, USB-Stick, usw.)</strong>
<other-images />
</div>
</div>

       
<p>
Alternativ können Sie unsere <b>Schnappschuss</b>-Images von Debian Testing verwenden.
Die wöchentlichen Builds erzeugen vollständige Sätze von Images, während die täglich
gebauten nur einzelne Images erstellen.
</p>

<div class="warning">

<p>
Diese Schnappschuss-Images installieren Debian Testing, aber der Installer basiert auf
Debian Unstable.
</p>

</div>

<h2>Aktueller wöchentlicher Schnappschuss</h2>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<devel-full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<devel-full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50 lastcol">
<strong>CD (via <a href="$(HOME)/CD/jigdo-cd">Jigdo</a>)</strong>
<devel-full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD (via <a href="$(HOME)/CD/jigdo-cd">Jigdo</a>)</strong>
<devel-full-dvd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray (via <a href="$(HOME)/CD/jigdo-cd">Jigdo</a>)</strong>
<devel-full-bd-jigdo />
</div>
</div>

</if-testing-installer>


<h2>Aktueller täglicher Schnappschuss</h2>

<div class="line">
<div class="item col50">
<strong>Netzinstallations-(netinst-)CD-Images</strong>
<devel-small-cd-images />
</div>

<div class="item col50 lastcol">
<strong>Netzinstallations-(netinst-)CD-Images (über <a 
     href="$(HOME)/CD/jigdo-cd">Jigdo</a>)</strong>
<devel-small-cd-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>Weitere Images (Netboot, USB-Stick usw.)</strong>
<devel-other-images />
</div>
</div>

<hr />

<p>
<b>Hinweise</b>
</p>
<ul>
# <li>Bevor Sie eines der täglich gebauten Images herunterladen, empfehlen wir
#     Ihnen, die <a href="https://wiki.debian.org/DebianInstaller/Today">bekannten
#     Fehler</a> zu überprüfen.</li>
 <li>Eine Architektur kann (temporär) von dem Überblick über täglich gebaute
     Images entfernt werden, falls diese täglichen Images nicht (zuverlässig) 
     gebaut werden können.</li>
 <li>Für die Installations-Images sind Prüfsummendateien (<tt>SHA512SUMS</tt>
     and <tt>SHA256SUMS</tt>) im gleichen
     Verzeichnis wie die Images verfügbar.</li>
 <li>Zum Herunterladen der kompletten CD- und DVD-Images wird die Verwendung 
     von Jigdo empfohlen.</li>
 <li>Nur eine begrenzte Anzahl von Images aus DVD-Sätzen ist für
     den direkten Download verfügbar. Die meisten Nutzer benötigen nicht all
     die Software, die auf den Disks enthalten ist; um Platz auf den
     Download-Servern und Spiegeln zu sparen, sind die vollständigen
     Sätze daher nur über Jigdo erhältlich.</li>
</ul>


<p>
<strong>Nach dem Benutzen des Debian-Installers</strong> senden Sie uns bitte einen
<a href="https://d-i.debian.org/manual/de.amd64/ch05s04.html#submit-bug">Installationsbericht</a>,
selbst falls keine Probleme auftraten.
</p>

<h1>Dokumentation</h1>

<p>
<strong>Falls Sie nur ein Dokument lesen</strong>, bevor Sie installieren, lesen Sie
unser
<a href="https://d-i.debian.org/manual/de.amd64/apa.html">\
Installations-Howto</a>, ein Schnelldurchgang des Installationsprozesses.
Weitere nützliche Dokumentation beinhaltet:
</p>

<ul>
<li>Installationsanleitung:
#    <a href="$(HOME)/releases/stable/installmanual">Version für
#       aktuelle Stable-Veröffentlichung</a>
#    &ndash;
    <a href="$(HOME)/releases/testing/installmanual">In
       Entwicklung befindliche Version (Testing)</a>
    &ndash;
    <a href="https://d-i.debian.org/manual/">letzte Version (Git)</a>
<br />
Detaillierte Installationsanweisungen</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">Debian-Installer-FAQ</a>
und <a href="$(HOME)/CD/faq/">Debian-CD-FAQ</a><br />
Häufige Fragen und Antworten</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer-Wiki</a><br />
Von der Gemeinschaft betreute Dokumentation</li>
</ul>

<h1>Kontakt</h1>

<p>Die <a href="https://lists.debian.org/debian-boot/">debian-boot-Mailingliste</a>
ist das Hauptforum für Diskussionen und Arbeit
am Debian-Installer.</p>

<p>Wir haben auch einen IRC-Kanal, #debian-boot, auf
<tt>irc.debian.org</tt>. Bitte beachten Sie, dass hier Englisch gesprochen
wird. Der Kanal wird hauptsächlich zur Entwicklung benutzt, manchmal
allerdings auch zur Unterstützung. Falls Sie keine Antwort erhalten,
versuchen Sie es stattdessen auf der Mailingliste.</p>
