#use wml::debian::template title="Debian 13 -- Errata" BARETITLE=true
#use wml::debian::toc

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Known problems</toc-add-entry>
<toc-add-entry name="security">Security issues</toc-add-entry>

<p>Debian security team issues updates to packages in the stable release
in which they've identified problems related to security. Please consult the
<a href="$(HOME)/security/">security pages</a> for information about
any security issues identified in <q>trixie</q>.</p>

<p>If you use APT, add the following line to <tt>/etc/apt/sources.list</tt>
to be able to access the latest security updates:</p>

<pre>
  deb http://security.debian.org/ trixie-security main contrib non-free non-free-firmware
</pre>

<p>After that, run <kbd>apt update</kbd> followed by
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Point releases</toc-add-entry>

<p>Sometimes, in the case of several critical problems or security updates, the
released distribution is updated.  Generally, these are indicated as point
releases.</p>

<!-- <ul>
  <li>The first point release, 13.1, was released on
      <a href="$(HOME)/News/2017/FIXME">FIXME</a>.</li>
</ul> -->

<ifeq <current_release_trixie> 13.0 "

<p>There are no point releases for Debian 13 yet.</p>" "

<p>See the <a
href="http://deb.debian.org/debian/dists/trixie/ChangeLog">\
ChangeLog</a>
for details on changes between 13 and <current_release_trixie/>.</p>"/>


<p>Fixes to the released stable distribution often go through an
extended testing period before they are accepted into the archive.
However, these fixes are available in the
<a href="http://ftp.debian.org/debian/dists/trixie-proposed-updates/">\
dists/trixie-proposed-updates</a> directory of any Debian archive
mirror.</p>

<p>If you use APT to update your packages, you can install
the proposed updates by adding the following line to
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# proposed additions for a 13 point release
  deb http://deb.debian.org/debian trixie-proposed-updates main contrib non-free
</pre>

<p>After that, run <kbd>apt update</kbd> followed by
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Installation system</toc-add-entry>

<p>
For information about errata and updates for the installation system, see
the <a href="debian-installer/">installation information</a> page.
</p>
