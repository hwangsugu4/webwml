#use wml::debian::template title="Информация о выпуске Debian &ldquo;buster&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/buster/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="6bafc9758f46d9e4fe5b48e5d85484d9f3dc8e3e" maintainer="Lev Lamberov"

<p>Debian <current_release_buster> был
выпущен <a href="$(HOME)/News/<current_release_newsurl_buster/>"><current_release_date_buster></a>.
<ifneq "10.0" "<current_release>"
  "Debian 10.0 изначально был выпущен <:=spokendate('2019-07-06'):>."
/>
Выпуск включает множество важных
изменений, описанных в
нашем <a href="$(HOME)/News/2019/20190706">анонсе</a> и
<a href="releasenotes">информации о выпуске</a>.</p>

<p><strong>Debian 10 был заменён на
<a href="../bullseye/">Debian 11 (<q>bullseye</q>)</a>.
Обновления безопасности прекращены с <:=spokendate('2022-06-30'):>.
</strong></p>

<p><strong>Однако buster имеет долгосрочную поддержку (LTS) до
конца июня 2024 года. Долгосрочная поддержка ограничена i386, amd64, armhf и arm64.
Все другие архитектуры более в buster по поддерживаются.
За дополнительной информацией обращайтесь к <a
href="https://wiki.debian.org/LTS">разделу LTS вики Debian</a>.
</strong></p>

<p>О том, как получить и установить Debian, см. страницу с
<a href="debian-installer/">информацией по установке</a> и
<a href="installmanual">руководство по установке</a>. Инструкции
по обновлению со старого выпуска см. в
<a href="releasenotes">информации о выпуске</a>.</p>

### Activate the following when LTS period starts.
#<p>В ходе долгосрочной поддержки поддерживаются следующие архитектуры:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>В этом выпуске поддерживаются следующие компьютерные архитектуры:</p>
# <p>Изначально в выпуске buster поддерживались следующие архиектуры:</p> ### Use this line when LTS starts, instead of the one above.

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Независимо от нашего желания в выпуске могут быть некоторые проблемы, несмотря на то, что он объявлен
<em>стабильным</em>. Мы составили
<a href="errata">список основных известных проблем</a>, и вы всегда можете
<a href="../reportingbugs">сообщить нам о других ошибках</a>.</p>

<p>Наконец, мы составили список <a href="credits">людей, которые внесли свой вклад</a>
в создание этого выпуска.</p>
