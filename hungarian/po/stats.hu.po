# SOME DESCRIPTIVE TITLE.
# Copyright ©
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2016-01-13 19:43+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Debian web oldal fordítási statisztikák"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "%d oldalt kell lefordítani."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "%d ájtot kell lefodítani."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "%d szót kell lefodítani."

#: ../../stattrans.pl:282 ../../stattrans.pl:495
msgid "Wrong translation version"
msgstr "Rossz fordítási verzió."

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "Az a fordítás nagyon elavult."

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "Az eredeti frissebb, mint ez a fordítás"

#: ../../stattrans.pl:290 ../../stattrans.pl:495
msgid "The original no longer exists"
msgstr "Az eredeti már nem létezik"

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr "találatok száma nem ismert"

#: ../../stattrans.pl:474
msgid "hits"
msgstr "találatok"

#: ../../stattrans.pl:600 ../../stattrans.pl:739
msgid "Created with <transstatslink>"
msgstr "Készült ezzel: "

#: ../../stattrans.pl:605
msgid "Translation summary for"
msgstr "Fordítási összefoglaló"

#: ../../stattrans.pl:608 ../../stattrans.pl:763 ../../stattrans.pl:809
#: ../../stattrans.pl:852
msgid "Not translated"
msgstr "Nincs lefordítva"

#: ../../stattrans.pl:608 ../../stattrans.pl:762 ../../stattrans.pl:808
msgid "Outdated"
msgstr "Elavult"

#: ../../stattrans.pl:608
msgid "Translated"
msgstr "Lefordított"

#: ../../stattrans.pl:608 ../../stattrans.pl:687 ../../stattrans.pl:761
#: ../../stattrans.pl:807 ../../stattrans.pl:850
msgid "Up to date"
msgstr "Naprakész"

#: ../../stattrans.pl:609 ../../stattrans.pl:610 ../../stattrans.pl:611
#: ../../stattrans.pl:612
msgid "files"
msgstr "fájlok"

#: ../../stattrans.pl:615 ../../stattrans.pl:616 ../../stattrans.pl:617
#: ../../stattrans.pl:618
msgid "bytes"
msgstr "bájtok"

#: ../../stattrans.pl:625
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Megjegyzés: az oldalak listája népszerűség szerint van rendezve. Menj az "
"egérrel az oldal neve fölé, hogy lásd a találatok számát."

#: ../../stattrans.pl:631
msgid "Outdated translations"
msgstr "Elavult fordítások"

#: ../../stattrans.pl:633 ../../stattrans.pl:686
msgid "File"
msgstr "File"

#: ../../stattrans.pl:635
msgid "Diff"
msgstr "Diff"

#: ../../stattrans.pl:637
msgid "Comment"
msgstr "Komment"

#: ../../stattrans.pl:638
msgid "Git command line"
msgstr "Git parancssor"

#: ../../stattrans.pl:640
msgid "Log"
msgstr "Log"

#: ../../stattrans.pl:641
msgid "Translation"
msgstr "Fordítás"

#: ../../stattrans.pl:642
msgid "Maintainer"
msgstr "Karbantartó"

#: ../../stattrans.pl:644
msgid "Status"
msgstr "Státusz"

#: ../../stattrans.pl:645
msgid "Translator"
msgstr "Fordító"

#: ../../stattrans.pl:646
msgid "Date"
msgstr "Dátum"

#: ../../stattrans.pl:653
msgid "General pages not translated"
msgstr "Nem lefordított általános oldalak"

#: ../../stattrans.pl:654
msgid "Untranslated general pages"
msgstr "Lefordítatlan általános oldalak"

#: ../../stattrans.pl:659
msgid "News items not translated"
msgstr "Nem lefordított új elemek"

#: ../../stattrans.pl:660
msgid "Untranslated news items"
msgstr "Lefordítatlan új elemek"

#: ../../stattrans.pl:665
msgid "Consultant/user pages not translated"
msgstr "Nem lefordított szaktanácsadói/felhasználói oldalak"

#: ../../stattrans.pl:666
msgid "Untranslated consultant/user pages"
msgstr "Lefordítattlan szaktanácsadói/felhasználói oldalak"

#: ../../stattrans.pl:671
msgid "International pages not translated"
msgstr "Nem lefordított nemzetközi oldalak"

#: ../../stattrans.pl:672
msgid "Untranslated international pages"
msgstr "Lefordítatlan nemzetközi oldalak"

#: ../../stattrans.pl:677
msgid "Translated pages (up-to-date)"
msgstr "Lefordított oldalak (naprakész)"

#: ../../stattrans.pl:684 ../../stattrans.pl:834
msgid "Translated templates (PO files)"
msgstr "Lefordított sablonok (PO fájlok)"

#: ../../stattrans.pl:685 ../../stattrans.pl:837
msgid "PO Translation Statistics"
msgstr "PO Fordítási statisztikák"

#: ../../stattrans.pl:688 ../../stattrans.pl:851
msgid "Fuzzy"
msgstr "Zavaros"

#: ../../stattrans.pl:689
msgid "Untranslated"
msgstr "Lefordítatlan"

#: ../../stattrans.pl:690
msgid "Total"
msgstr "Összesen"

#: ../../stattrans.pl:707
msgid "Total:"
msgstr "Összesen:"

#: ../../stattrans.pl:741
msgid "Translated web pages"
msgstr "Lefordított web oldalak"

#: ../../stattrans.pl:744
msgid "Translation Statistics by Page Count"
msgstr "Fordítási Statisztikák a Page Count által"

#: ../../stattrans.pl:759 ../../stattrans.pl:805 ../../stattrans.pl:849
msgid "Language"
msgstr "Nyelv"

#: ../../stattrans.pl:760 ../../stattrans.pl:806
msgid "Translations"
msgstr "Fordítások"

#: ../../stattrans.pl:787
msgid "Translated web pages (by size)"
msgstr "Lefordított web oldalak (méret szerint)"

#: ../../stattrans.pl:790
msgid "Translation Statistics by Page Size"
msgstr "Fordítási statisztikák az oldal mérete szerint"

#~ msgid "Click to fetch diffstat data"
#~ msgstr "Kattints a diffstat adatok begyűjtéséhez"

#~ msgid "Diffstat"
#~ msgstr "Diffstat"
