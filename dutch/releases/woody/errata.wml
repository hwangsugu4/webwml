#use wml::debian::template title="Debian GNU/Linux 3.0 -- Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="534d1b782cfb92f46dc41fd064f779fffc329b12"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>




<toc-add-entry name="security">Beveiligingsproblemen</toc-add-entry>

<p>Het Debian veiligheidsteam brengt updates uit voor pakketten uit de stabiele
release waarin problemen in verband met beveiliging vastgesteld werden.
Raadpleeg de <a href="$(HOME)/security/">beveiligingspagina's</a> voor informatie
over eventuele beveiligingsproblemen die in `woody' ontdekt werden.</p>

<p>Als u APT gebruikt, voeg dan de volgende regel toe aan <tt>/etc/apt/sources.list</tt>
om toegang te hebben tot de laatste beveiligingsupdates:</p>

<pre>
  deb http://security.debian.org/ woody/updates main contrib non-free
</pre>

<p>Voer daarna <kbd>apt-get update</kbd> uit, gevolgd door
<kbd>apt-get upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Tussenreleases</toc-add-entry>

<p>Soms, in het geval van diverse kritieke problemen of beveiligingsupdates,
wordt de uitgebrachte distributie bijgewerkt. Een
dergelijke bijwerking wordt gewoonlijk aangeduid als een tussenrelease.</p>

<ul>
  <li>De eerste tussenrelease, 3.0r1, werd uitgebracht op
    <a href="$(HOME)/News/2002/20021216">16 december 2002</a>.</li>
  <li>De tweede tussenrelease, 3.0r2, werd uitgebracht op
    <a href="$(HOME)/News/2003/20031121a">21 november 2003</a>.</li>
  <li>De derde tussenrelease, 3.0r3, werd uitgebracht op
    <a href="$(HOME)/News/2004/20041026">26 oktober 2004</a>.</li>
  <li>De vierde tussenrelease, 3.0r4, werd uitgebracht op
    <a href="$(HOME)/News/2005/20050101">1 januari 2005</a>.</li>
  <li>De vijfde tussenrelease, 3.0r5, werd uitgebracht op
    <a href="$(HOME)/News/2005/20050416">16 april 2005</a>.</li>
  <li>De zesde tussenrelease, 3.0r6, werd uitgebracht op
    <a href="$(HOME)/News/2005/20050602">2 juni 2005</a>.</li>
</ul>

<ifeq <current_release_woody> 3.0r0 "

<p>Er zijn nog geen tussenreleases voor Debian 3.0.</p>" "

<p>Zie de <a
href=http://archive.debian.org/debian/dists/woody/ChangeLog>
ChangeLog</a> (en de <a
href=http://archive.debian.org/debian-non-US/dists/woody/non-US/ChangeLog>
ChangeLog voor non-US</a>) voor details over wijzigingen tussen 3.0r0 en
<current_release_woody/>.</p>"/>

<p>Verbeteringen voor de uitgebrachte stabiele distributie gaan dikwijls door een
uitgebreide testperiode voordat ze in het archief worden aanvaard.
Deze verbeteringen zijn echter wel beschikbaar in de map
<a href="http://archive.debian.org/debian/dists/woody-proposed-updates/">
dists/woody-proposed-updates</a> van elke Debian archief-spiegelserver
(en op dezelfde locatie op onze
<a href="http://archive.debian.org/debian-non-US/dists/woody-proposed-updates/">
non-US server</a> en zijn spiegelservers).</p>

<p>Als u <tt>apt</tt> gebruikt om uw pakketten bij te werken, dan kunt u de
voorgestelde updates installeren door de volgende regel toe te voegen aan
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# voorgestelde toevoegingen aan een tussenrelease van 3.0
  deb http://archive.debian.org/debian proposed-updates main contrib non-free
  deb http://archive.debian.org/debian-non-US proposed-updates/non-US main contrib non-free
</pre>

<p>Voer daarna <kbd>apt-get update</kbd> uit, gevolgd door
<kbd>apt-get upgrade</kbd>.</p>


<toc-add-entry name="diskcontroller">Niet-officiële ondersteuning voor schijfcontrollers die niet worden gedetecteerd door het installatiesysteem</toc-add-entry>

<p>Enkele systemen met RAID-controllers zoals Adaptec-2400A worden niet ondersteund door het standaard installatiesysteem. U kunt Debian 3.0 nog steeds
installeren met behulp van de bf2.4-variant en door vooraf stuurprogrammamodules
te laden van deze
<a href="https://people.debian.org/~blade/install/preload/">diskette</a>.</p>
