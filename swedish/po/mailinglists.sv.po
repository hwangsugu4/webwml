#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: (null)\n"
"PO-Revision-Date: 2014-05-18 18:34+0100\n"
"Last-Translator: Anders Jonsson <anders.jonsson@norsjovallen.se>\n"
"Language-Team: debian-l10n-swedish@lists.debian.org <sv@li.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.4\n"

#: ../../english/MailingLists/mklist.tags:6
msgid "Mailing List Subscription"
msgstr "Prenumerera på sändlistor"

#: ../../english/MailingLists/mklist.tags:9
msgid ""
"See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
"how to subscribe using e-mail. An <a href=\"unsubscribe\">unsubscription web "
"form</a> is also available, for unsubscribing from mailing lists. "
msgstr ""
"Se <a href=\"./#subunsub\">sändlistesidan</a> för information om hur du "
"prenumererar via e-post. Det finns även en <a href=\"unsubscribe\">webbsida "
"för avbeställning</a> av sändlistorna. "

#: ../../english/MailingLists/mklist.tags:12
msgid ""
"Note that most Debian mailing lists are public forums. Any mails sent to "
"them will be published in public mailing list archives and indexed by search "
"engines. You should only subscribe to Debian mailing lists using an e-mail "
"address that you do not mind being made public."
msgstr ""
"Observera att de flesta Debiansändlistor är öppna fora. All e-post som sänds "
"till dem kommer att publiceras i öppna sändlistearkiv och indexeras av "
"sökmaskiner. Du bör endast prenumerera på Debiansändlistor med en e-"
"postadress du inte har något emot att den blir allmänt känd."

#: ../../english/MailingLists/mklist.tags:15
msgid ""
"Please select which lists you want to subscribe to (the number of "
"subscriptions is limited, if your request doesn't succeed, please use <a "
"href=\"./#subunsub\">another method</a>):"
msgstr ""
"Vänligen välj vilka listor du vill prenumerera på (antalet prenumerationer "
"är begränsat, om din förfrågan inte lyckas, välj vänligen en <a href=\"./"
"#subunsub\">annan metod</a>):"

#: ../../english/MailingLists/mklist.tags:18
msgid "No description given"
msgstr "Beskrivning saknas"

#: ../../english/MailingLists/mklist.tags:21
msgid "Moderated:"
msgstr "Modererad:"

#: ../../english/MailingLists/mklist.tags:24
msgid "Posting messages allowed only to subscribers."
msgstr "Tillåter endast prenumeranter att sända inlägg."

#: ../../english/MailingLists/mklist.tags:27
msgid ""
"Only messages signed by a Debian developer will be accepted by this list."
msgstr "Listan godtar bara brev signerade av Debianutvecklare."

#: ../../english/MailingLists/mklist.tags:30
msgid "Subscription:"
msgstr "Prenumeration:"

#: ../../english/MailingLists/mklist.tags:33
msgid "is a read-only, digestified version."
msgstr "är en skrivskyddad variant där sammanfattningar postas."

#: ../../english/MailingLists/mklist.tags:36
msgid "Your E-Mail address:"
msgstr "Din e-postadress:"

#: ../../english/MailingLists/mklist.tags:39
msgid "Subscribe"
msgstr "Prenumerera"

#: ../../english/MailingLists/mklist.tags:42
msgid "Clear"
msgstr "Återställ"

#: ../../english/MailingLists/mklist.tags:45
msgid ""
"Please respect the <a href=\"./#ads\">Debian mailing list advertising "
"policy</a>."
msgstr ""
"Vänligen respektera <a href=\"./#ads\">Debians annonseringspolicy för "
"sändlistorna</a>."

#: ../../english/MailingLists/mklist.tags:48
msgid "Mailing List Unsubscription"
msgstr "Avbeställa sändlistor"

#: ../../english/MailingLists/mklist.tags:51
msgid ""
"See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
"how to unsubscribe using e-mail. An <a href=\"subscribe\">subscription web "
"form</a> is also available, for subscribing to mailing lists. "
msgstr ""
"Se <a href=\"./#subunsub\">sändlistesidan</a> för information om hur du "
"avbeställer via e-post. Det finns även en <a href=\"subscribe\">webbsida för "
"att prenumerera</a> på sändlistorna. "

#: ../../english/MailingLists/mklist.tags:54
msgid ""
"Please select which lists you want to unsubscribe from (the number of "
"unsubscriptions is limited, if your request doesn't succeed, please use <a "
"href=\"./#subunsub\">another method</a>):"
msgstr ""
"Vänligen välj vilka listor du vill avanmäla dig från (antal avanmälningar är "
"begränsat, om din förfrågan inte lyckas, välj vänligen en <a href=\"./"
"#subunsub\">annan metod</a>):"

#: ../../english/MailingLists/mklist.tags:57
msgid "Unsubscribe"
msgstr "Avbeställ"

#: ../../english/MailingLists/mklist.tags:60
msgid "open"
msgstr "öppen"

#: ../../english/MailingLists/mklist.tags:63
msgid "closed"
msgstr "stängd"

#~ msgid "Please select which lists you want to subscribe to:"
#~ msgstr "Välj vilka listor du vill prenumerera på:"

#~ msgid "Please select which lists you want to unsubscribe from:"
#~ msgstr "Välj de listor du vill avbeställa:"
