#use wml::debian::template title="Πληροφορίες κυκλοφορίας του Debian &ldquo;bullseye&rdquo;" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#use wml::debian::translation-check translation="3ef6ad53634d4f4729a6f8c44cc85db8f5fe79c0" maintainer="galaxico"

<h1>Εγκαθιστώντας το Debian <current_release_bullseye></h1>

<if-stable-release release="bookworm">
<p><strong>Το Debian 11 έχει αντικατασταθεί από την έκδοση
<a href="../../bookworm/">Debian 12 (<q>bookworm</q>)</a>. Μερικές από αυτές τις
εικόνες εγκατάστασης ίσως να μην είναι πλέον διαθέσιμες ή ίσως να μην δουλεύουν
και σας συνιστούμε να εγκαταστήσετε αντί για αυτήν την έκδοση bookworm.
</strong></p>
</if-stable-release>

<p>
<strong>To install Debian</strong> <current_release_bullseye>
(<em>bullseye</em>), download any of the following images (all i386 and amd64
CD/DVD images can be used on USB sticks too):
</p>

<div class="line">
<div class="item col50">
	<p><strong>netinst CD image (generally 150-280 MB)</strong></p>
		<netinst-images />
</div>


</div>

<div class="line">
<div class="item col50">
	<p><strong>full CD sets</strong></p>
		<full-cd-images />
</div>

<div class="item col50 lastcol">
	<p><strong>full DVD sets</strong></p>
		<full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-cd-torrent />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-dvd-torrent />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<p><strong>Blu-ray  (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-bluray-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>other images (netboot, flexible usb stick, etc.)</strong></p>
<other-images />
</div>
</div>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>
If any of the hardware in your system <strong>requires non-free firmware to be
loaded</strong> with the device driver, you can use one of the
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/bullseye/current/">\
tarballs of common firmware packages</a> or download an <strong>unofficial</strong> image including these <strong>non-free</strong> firmwares. Instructions how to use the tarballs
and general information about loading firmware during an installation can
be found in the <a href="../amd64/ch06s04">Installation Guide</a>.
</p>
<div class="line">
<div class="item col50">
<p><strong>netinst (generally 240-290 MB) <strong>non-free</strong>
CD images <strong>with firmware</strong></strong></p>
<small-non-free-cd-images />
</div>
</div>
</div>

<p>
<strong>Σημειώσεις</strong>
</p>
<ul>
    <li>
	Για να κατεβάσετε πλήρεις εικόνες CD και DVD συνιστάται η χρήση BitTorrent ή jigdo.
    </li><li>
	Για τις λιγότερο συνηθισμένες αρχιτεκτονικές, είναι διαθέσιμος ένας περιορισμένος μόνο αριθμός
	εικόνων από τα σετ CD και DVD ως αρχείο ISO ή μέσω BitTorrent.
	Τα πλήρη σετ είναι διαθέσιμα μόνο μέσω The full sets are only available via jigdo.
    </li><li>
    	Οι εικόνες <em>CD</em> πολλαπλών αρχιτεκτονικών υπσστηρίζουν μόνο τις i386/amd64· η εγκατάσταση είναι παρόμοια με 
    	την εγκατάσταση από μια εικόνα netinst για μια μόνο αρχιτεκτονική.
    </li><li>
	Οι εικόνες <em>DVD</em> πολλαπλών αρχιτεκτονικών υποστηρίζουν τις i386/amd64· η 
	εγκατάσταση είναι παρόμοια με την εγκατάσταση από μια πλήρη εικόνα
	CD για μια μόνο αρχιτεκτονική· το DVD περιλαμβάνει επίσης τον πηγαίο κώδικα για 
	όλα τα πακέτα που περιλαμβάνει.
    </li><li>    
	Για τις εικόνες εγκατάστασης, είναι διαθέσιμα αρχεία επαλήθευσης (<tt>SHA256SUMS</tt>,
	<tt>SHA512SUMS</tt> και άλλα) από τον ίδιο κατάλογο στον οποίο βρίσκονται και οι εικόνες.
    </li>
</ul>


<h1>Τεκμηρίωση</h1>

<p>
<strong>Αν χρειάζεστε ένα μοναδικό κείμενο </strong> πριν την εγκατάσταση, διαβάστε το
<a href="../i386/apa">Το πώς της Εγκατάστασης</a>, μια γρήγορη περιήγηση στη διαδικασία
εγκατάστασης. Άλλες πηγές χρήσιμης τεκμηρίωσης περιλαμβάνουν:
</p>

<ul>
<li><a href="../installmanual">Οδηγός Εγκατάστασης της έκδοσης Bullseye</a><br />
λεπτομερείς οδηγίες εγκατάστασης</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">Συχνές ερωτήσεις του Εγκαταστάτη του Debian</a>
και <a href="$(HOME)/CD/faq/">Debian-CD FAQ</a><br />
συχνές ερωτήσεις και απαντήσεις</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Wiki του Εγκαταστάτη του Debian</a><br />
τεκμηρίωση που συντηρείται από την κοινότητα</li>
</ul>


<h1 id="errata">Παροράματα</h1>

<p>
Αυτή είναι μια λίστα γνωστών προβλημάτων του Εγκαταστάτη που έρχεται
με την έκδοση Debian <current_release_bookworm>. Άν έχετε εμπειρία ενός προβλήματος
εγκαθιστώντας το Debian και δεν το βλέπετε να αναφέρεται εδώ, παρακαλούμε στείλτε μας 
μια <a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">αναφορά εγκατάστασης</a>
περιγράφοντας το πρόβλημα ή 
<a href="https://wiki.debian.org/DebianInstaller/BrokenThings">ελέγξτε το wiki</a>
για άλλα γνωστά προβλήματα.
</p>


## Translators: copy/paste from devel/debian-installer/errata
<h3 id="errata-r0">Παροράματα για την έκδοση 11.0</h3>

<dl class="gloss">

     <dt>Firmwares required for some sound cards</dt>
     <dd>There seems to be a number of sound cards that require loading a
     firmware to be able to emit sound. As of Bullseye, the installer is
     not able to load them early, which means that speech synthesis during
     installation is not possible with such cards. A possible workaround is to
     plug another sound card which does not need such firmware.
     See the <a href="https://bugs.debian.org/992699">umbrella bug report</a>
     to keep track of our efforts.</dd>

<!--
     <dt>Desktop installations may not work using CD#1 alone</dt>

     <dd>Due to space constraints on the first CD, not all of the
     expected GNOME desktop packages fit on CD#1. For a successful
     installation, use extra package sources (e.g. a second CD or a
     network mirror) or use a DVD instead.

     <br /> <b>Status:</b> It is unlikely more efforts can be made to
     fit more packages on CD#1. </dd>
-->
</dl>


<p>
Βελτιωμένες εκδόσεις του συστήματος εγκαταστήσετες αναπτύσσονται για την επόμενη 
έκδοση του Debian, και μπορούν επίσης να χρησιμοποιηθούν για να εγκαταστήσετε την 
έκδοση bookworm. Για λεπτομέρειες δείτε τη σελίδα 
<a href="$(HOME)/devel/debian-installer/">Σελίδα του σχεδίου του Εγκαταστάτη του Debian</a>.
</p>
