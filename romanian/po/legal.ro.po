msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2016-09-20 12:17+0100\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.8\n"

#: ../../english/template/debian/legal.wml:15
msgid "License Information"
msgstr "Despre licența"

#: ../../english/template/debian/legal.wml:19
msgid "DLS Index"
msgstr "Index DLS"

#: ../../english/template/debian/legal.wml:23
msgid "DFSG"
msgstr "DFSG"

#: ../../english/template/debian/legal.wml:27
msgid "DFSG FAQ"
msgstr "DFSG -Întrebări frecvente"

#: ../../english/template/debian/legal.wml:31
msgid "Debian-Legal Archive"
msgstr "Arhiva Debian-Legal"

#. title string without version, on the form "dls-xxx - license name: status"
#: ../../english/template/debian/legal.wml:49
msgid "%s  &ndash; %s: %s"
msgstr "%s  &ndash; %s: %s"

#. title string with version, on the form "dls-xxx - license name, version: status"
#: ../../english/template/debian/legal.wml:52
msgid "%s  &ndash; %s, Version %s: %s"
msgstr "%s  &ndash; %s, Versiune %s: %s"

#: ../../english/template/debian/legal.wml:59
msgid "Date published"
msgstr "Data publicării"

#: ../../english/template/debian/legal.wml:61
msgid "License"
msgstr "Licență"

#: ../../english/template/debian/legal.wml:64
msgid "Version"
msgstr "Versiune"

#: ../../english/template/debian/legal.wml:66
msgid "Summary"
msgstr "Cuprins"

#: ../../english/template/debian/legal.wml:70
msgid "Justification"
msgstr "Justificare"

#: ../../english/template/debian/legal.wml:72
msgid "Discussion"
msgstr "Discuție"

#: ../../english/template/debian/legal.wml:74
msgid "Original Summary"
msgstr "Rezumat inițial"

#: ../../english/template/debian/legal.wml:76
msgid ""
"The original summary by <summary-author/> can be found in the <a href="
"\"<summary-url/>\">list archives</a>."
msgstr ""
"Rezumatul inițial creat de <summary-author/> poate fi găsit in <a href="
"\"<summary-url/>\">lista de arhive</a>."

#: ../../english/template/debian/legal.wml:77
msgid "This summary was prepared by <summary-author/>."
msgstr "Acest rezumat a fost creeat de către <summary-author/>."

#: ../../english/template/debian/legal.wml:80
msgid "License text (translated)"
msgstr "Textul licenței (tradus)"

#: ../../english/template/debian/legal.wml:83
msgid "License text"
msgstr "Textul licenței"

#: ../../english/template/debian/legal_tags.wml:6
msgid "free"
msgstr "liber"

#: ../../english/template/debian/legal_tags.wml:7
msgid "non-free"
msgstr "non-liber"

#: ../../english/template/debian/legal_tags.wml:8
msgid "not redistributable"
msgstr "ce nu poate fi redistribuit"

#. For the use in headlines, see legal/licenses/byclass.wml
#: ../../english/template/debian/legal_tags.wml:12
msgid "Free"
msgstr "Liber"

#: ../../english/template/debian/legal_tags.wml:13
msgid "Non-Free"
msgstr "Non-liber"

#: ../../english/template/debian/legal_tags.wml:14
msgid "Not Redistributable"
msgstr "Ce nu poate fi redistribuit"

#: ../../english/template/debian/legal_tags.wml:27
msgid ""
"See the <a href=\"./\">license information</a> page for an overview of the "
"Debian License Summaries (DLS)."
msgstr ""
"Vezi pagina <a href=\"./\">despre licență</a> dacă dorești sa accesezi "
"Rezumatul Licențelor Debian (Debian License Summaries, sau DLS)."
